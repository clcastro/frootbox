var AWS = require('aws-sdk'),
    phantom = require('phantom'),
    imagemagick = require('imagemagick-native'),
    FROOTCONFIG = require('config'),
    mongoose = require('mongoose'),
    User = require('./models/user'),
    Plan = require('./models/plan'),
    Account = require('./models/account'),
    AccountPlan = require('./models/accountplan'),
    UserAccount = require('./models/useraccount'),
    Minion = require('./models/minion'),
    BluePrint = require('./models/blueprint'),
    TestRequest = require('./models/testrequest'),
    Test = require('./models/test'),
    Page = require('./models/page'),
    Task = require('./models/task'),
    Crawl = require('./models/crawl'),
    crawljob = require('./lib/crawljob'),
    taskjob = require('./lib/taskjob'),
    fs = require('fs');

AWS.config.loadFromPath('./config/config-aws.json');
var bucket = FROOTCONFIG.aws.bucket;
var s3 = new AWS.S3();

//First we get the TestRequest

TestRequest.onCreate(function (err,rq) {
    console.log("TestRequest received");
    if(!rq){
        return console.log("empty request",rq);
    }
    var now = new Date();
    var secondsFromCreation = (rq.created.getTime() - now.getTime())/1000;
    rq.isBeingProcessed = true;
    rq.save(function(err,request){  console.log("TestRequest was marked as being processed");  });

    var url = rq.url;
    if(url.indexOf("http://") < 0 && url.indexOf("https://") < 0){
        url = "http://" + url;
    }
    console.log("request found:",url);
    console.log("creating phantom");
    var resources = {};
    var documentHTML = "";
    var documentContentType = "text/text";
    var documentStatus = 0;
    var documentStatusText = 0;
    var documentHeaders = [];
    phantom.create(function (ph) {
      console.log("> phantom created");
      console.log("> phantom create page");
      ph.createPage(function (page) {
        console.log(">> phantom page created");
        console.log(">> phantom opening url",url);

        page.set('onResourceReceived', function (res) {
            resources[res.url] = res;
        })
        page.set('viewportSize', {width:rq.width,height:rq.height}, function(){

            console.log(">>>viewportSize",arguments);
            page.set('clipRect', { top: 0, left: 0, width:rq.width, height: rq.height }, function(){
                console.log(">>>>clipRect",arguments);
                page.open(url, function (status) {
                    console.log(">>>>> phantom url opened",status);
                    page.get("content",function(content){
                        documentHTML = content;
                        page.evaluate(function () { return document.documentURI; }, function (result) {

                            documentStatus = resources[result].status ;
                            documentStatusText = resources[result].statusText ;
                            documentContentType = resources[result].contentType ;
                            documentHeaders = resources[result].headers ;
                            if(documentStatus == 200){
                                    var now = new Date();
                                    var imageID = Math.floor(Math.random() * 10) + parseInt(now.getTime()).toString(36).toUpperCase()
                                    var imagePath = "./temp/"+imageID+".jpg";
                                    console.log(">>>>>>preparing to render");
                                    page.render(imagePath, { format: "jpg", quality: 80 },function(){
                                        console.log(">>>>>>>render",imagePath);
                                        ph.exit();
                                        processTestRequest(rq,secondsFromCreation,url,result,documentHTML,documentStatus,documentStatusText,documentContentType,documentHeaders,imageID,imagePath);
                                    });

                            }
                            else{
                                ph.exit();
                                processTestRequest(rq,secondsFromCreation,url,result,documentHTML,documentStatus,documentStatusText,documentContentType,documentHeaders);
                            }


                        });


                    });


                });
            });
        });
      });
    });


});


//OK now we have the response from phantomjs and we need to create the process
var processTestRequest = function(testRequestObj,secondsFromCreation,callUrl,finalUrl,html,documentStatus,documentStatusText,documentContentType,documentHeaders,imageID,imagePath){
    if(documentStatus != 200)
    {
         testRequestObj.wasTestCreated = false;
         testRequestObj.httpStatus = documentStatus;
         testRequestObj.secondsUntilProcessed = secondsFromCreation;
         testRequestObj.save(function(err,rq){
            sockForWeb.send("testrequest|"+JSON.stringify({id:rq._id,status:"error",statusCode:documentStatus}));
         });
    }
    else{

        var test = new Test({
             //accountplanid : testRequestObj.accountplanid,
             userid : testRequestObj.userid,
             bluePrintId : testRequestObj.bluePrintId,
             //userBluePrintId : testRequestObj.userBluePrintId,
             created: new Date(),
             status: "created",
             callUrl: finalUrl,
             finalUrl: finalUrl,
             homeContentType: documentContentType,
             homeHeaders: documentHeaders,
             homeHtml: html
         });
        test.save(function(err,test){
            if(err) return console.log("error saving the test",err);


            if(imagePath){
                try{
                    var srcData  = fs.readFileSync(imagePath);
                    var fileStats =  fs.statSync(imagePath)
                }catch(e){
                    console.log("exec-read-error",e);
                }
                var screenshotData =  imagemagick.identify({srcData: srcData });

                var filekey = imageID+".jpg";
                s3.client.putObject({
                    Bucket: bucket,
                    Key: filekey,
                    Body: srcData,
                    ContentType: 'image/jpeg'
                  },function (err1) {
                      console.log(bucket,filekey);
                      console.log(arguments);
                      if(err1){ return console.error("Error saving the image",err1); }

                      fs.unlink(imagePath, function(err){
                          if (err) return console.log("Error removing the image");
                      });
                      test.homeBigImageUrl = "https://s3-us-west-2.amazonaws.com/"+bucket+"/"+filekey
                      test.save(function(err,test){
                        if(err) return console.log("error saving the image for the test",err);

                          var now = new Date();
                          var secondsWhenCreated = (now.getTime() - testRequestObj.created.getTime())/1000;
                          associateTestResponseWithTestAndContinue(testRequestObj,test,secondsFromCreation,secondsWhenCreated);

                      });
                  });
            }
            else{
                var now = new Date();
                var secondsWhenCreated = (now.getTime() - testRequestObj.created.getTime())/1000;
                associateTestResponseWithTestAndContinue(testRequestObj,test,secondsFromCreation,secondsWhenCreated);

            }
        });
    }
};

var associateTestResponseWithTestAndContinue = function(testRequestObj,testObj,secondsFromCreation,secondsWhenCreated){
    testRequestObj.wasTestCreated = true;
    testRequestObj.testid = testObj;
    testRequestObj.isBeingProcessed = false;
    testRequestObj.secondsUntilProcessed = secondsFromCreation;
    testRequestObj.secondsUntilTestCreated = secondsWhenCreated;

    testRequestObj.setStatusCompleted(function(err){
       startTestProcessing(testObj);
    });
}



var startTestProcessing = function(test){
/**** PASOS ***
1 - leemos el blueprint asociado al test, y sacamos la lista de minions a ejecutar.
2 - Guardamos la información de la portada que obtuvimos con el testrequest
3 - Generamos la lista de tareas para la portada usando la lista que ya tenemos.
4 - Comenzamos a llamar a cada uno de los minions para la portada, guardando sus respuestas.
5 - Mientras tanto mandamos a hacer el crawling.
6 - Una vez tenemos listo el resultado del crawling generamos la lista de tareas para cada una de las paginas obtenidas en el crawling
7 - Llamamos a cada uno de los minions para todo el resto del sitio.
8 - En caso de que hubieran llamados a minions de site los llamamos al principio o cuando ya hayan cargado todas las páginas de acuerdo a lo que este configurado.
9 -
*****************/
    //[1]
    BluePrint.findOne({_id: test.bluePrintId},function(err,bluep){
        if(err)
            return console.log("Error finding the blueprint",err);

        if(!bluep)
            return console.log("Empty blueprint");
        else
        {
            //[2]creamos la pagina de la portada - no importa que sea asincronica, porque puede correr paralelo a la siguiente tarea
            var frontpage = new Page({
                                        testid: test._id,
                                        userid: test.userid,
                                        url: test.finalUrl,
                                        contentType: test.homeContentType,
                                        headers: test.homeHeaders,
                                        html: test.homeHtml,
                                        created:new Date()
                                    });
            frontpage.save(function(err,page){
                            if(err)
                                return console.log("Error creating the homepage",err);
                            test.homeHtml = "";
                            test.homeHeaders = "";
                            test.pages = [{pageid:page._id,isHome:true}];
                            test.save(function(){
                                test.addlog(20001,"Home created",{pageid:page._id},function(err,task){});
                            });


                            //[3] generamos la lista de tarea utilizando el blueprint
                            console.log("creating tasks for url:",page.url);
                            bluep.homeMinions.map(function(item){
                                                    var tsk = new Task({
                                                                        testid: test._id,
                                                                        userid: test.userid,
                                                                        pageid: page._id,
                                                                        created: new Date(),
                                                                        minion: item.slug,
                                                                        size: item.size,
                                                                        config: item.config?item.config:{},//by now
                                                                        status: 'created'
                                                                        });
                                                    tsk.save(function(err,task){
                                                        console.log("new task [",item.slug,"] for url:",page.url);
                                                    });
                                                });
                        });
            //[5] mandamos a hacer el crawling
           /*** var crawl = new Crawl({
                                        testid: test._id,
                                        userid: test.userid,
                                        url: test.finalUrl,
                                        limit:bluep.maxPages,
                                        created:new Date()

                                    });
            crawl.save(function(error,crawl){
                            if(error){  return console.error("There was an error creating the crawl order"); }
                            test.addlog(20002,"Crawl order created",{crawlid:crawl._id},function(err,task){});
                            console.log("Crawl created for url [",test.finalUrl,"]");
                        });****/
        }

    });

}
//[4]
Task.onCreate(taskjob.processNewTask);


//[6] - procesamos la nueva solicitud de crawling esto podria hacerse en otro script pero por simplificar lo haremos aca por ahora
Crawl.onCreate(crawljob.processNewCrawl);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
mongoose.connect(FROOTCONFIG.db.mongo);
console.info("STARTING CENTRAL");

/*Test.findById('53c33310b7a3275c54000002',function(err,test){
    startTestProcessing(test);
});*/
