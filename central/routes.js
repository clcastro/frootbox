var http = require('follow-redirects').http;

var https = require('follow-redirects').https;
    $ = require('jquery'),    
    url = require('url'),
    validator = require('validator'),
    FROOTCONFIG = require('config');

//Models
var User = require('../webroot/models/user'),
    Plan = require('../webroot/models/plan'),
    Account = require('../webroot/models/account'),
    AccountPlan = require('../webroot/models/accountplan'),
    UserAccount = require('../webroot/models/useraccount'),
    Minion = require('../webroot/models/minion'),    
    BluePrint = require('../webroot/models/blueprint'),    
    TestRequest = require('../webroot/models/testrequest'),    
    Test = require('../webroot/models/test');

module.exports = function (app) {
};