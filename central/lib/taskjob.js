var Crawl = require('../models/crawl'),
    Page = require('../models/page'),
    Test = require('../models/test'),
    Task = require('../models/task'),
    exec = require('child_process').exec,
	fs = require('fs'),
    http = require('http'),
    querystring = require('querystring'),
    request = require('request'),
    url = require('url');

exports.processNewTask = function(error,task){      
    console.log("New task:",task.minion);    
    task.setStatusActive(function(error,task){
        Page.findById(task.pageid,function(err,page){
            request.post(
        'http://www.frootbox.com:7777/minion/'+task.minion+'/execute/test_job',
        { form: {
                  //'job-id' : task._id + "",
                  'type': 'page',
                  'url': page.url
                  //'html': page.html
              }},
        function (error, response, body) {
            try{
                if(error){
                    task.setStatusFailed(function(err,t){             
                       console.log("error on minion call");
                       var entry = {
                                        code: 30011,
                                        message: "Minion "+task.minion+" error",
                                        relatedids: { pageid: task.pageid }
                                    };
                        Test.findByIdAndUpdate(
                                        task.testid,
                                        {$push: {log: entry},lastModified:new Date() },
                                        {safe: true, upsert: true},
                                        function(err, task) {

                                        }
                                    );                                              
                     
                    
                    });
                }
                else if (!error && response && response.statusCode == 200) {
                    //console.log(body)                    console.log("ok",task.minion);
                    try{
                        var jsondata = JSON.parse(body);                    
                    }
                    catch(e){	
                        console.log("Error parsing");
                        return task.setStatusFailed(function(err,t){ console.log("failed, because of parsing", e);  });                        
                    }
                    console.log("we have json response",task.minion,task.pageid);
                    Page.findByIdAndUpdate(
                        task.pageid,
                        {lastModified:new Date() ,$push: {minions: {minion:task.minion,data: jsondata}}},
                        {safe: true, upsert: true},
                        function(err, page) {
                                console.log("ok page");
                            
                                   var entry = {
                                                    code: 30010,
                                                    message: "Minion "+task.slug+" completed",
                                                    relatedids: { pageid: task.pageid }
                                                };
                                    Test.findByIdAndUpdate(
                                                    task.testid,
                                                    {$push: {log: entry},lastModified:new Date() },
                                                    {safe: true, upsert: true},
                                                    function(err, task) {
                                                        
                                                    }
                                                );                                              
                                return task.setStatusCompleted(function(err,t){ console.log("completed");                                              
                            });
                        }
                    );

                }
            }
            catch(e){
               return task.setStatusFailed(function(err,t){ console.log("failed, because of try-catch", e);  });
            }
        }
    );

        });
    });
}