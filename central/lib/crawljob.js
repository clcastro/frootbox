var Crawl = require('../models/crawl'),
    Page = require('../models/page'),
    Test = require('../models/test'),
    Task = require('../models/task'),
    exec = require('child_process').exec,
	fs = require('fs'),
    
    url = require('url');

exports.processNewCrawl = function(error,crawl){      
    console.log("New crawling job:",crawl);
    Test.findById(crawl.testid)
        .populate('bluePrintId')
        .select('bluePrintId')
        .exec(function (err, test) {
          if (err) return console.log(err);
          var bluep = (test.bluePrintId);
            

            var hostname = url.parse(crawl.url).hostname;
            var filename = 'resultfile-'+crawl.testid +".json";

            var cmd = "casperjs --file-name="+filename+" --start-url="+crawl.url+" --required-values="+hostname+" --limit="+crawl.limit+"  --log-level=warning --verbose=true ./casperjs-spider/spider.js"
            console.log("Starting crawler")
            var child = exec( cmd,{},function (error, stdout, stderr){
                console.log("Crawl completed");
                if(error !== null){
                    crawl.status = 'error';
                    crawl.save(function(err,crl){
                        if(err){
                                return console.log("Error saving the completed crawl"); 
                        }
                        return console.log("Crawl completed saved");                           
                    });
                }
                else if(error === null){

                    fs.readFile(require('path').resolve( __dirname,  '../logs/'+filename ) , 'utf8', function (err, data) {
                      if (err) {
                        console.log('Error: ' + err);
                        return;
                      }
                      data = JSON.parse(data);
                        
                      crawl.foundPages = data.links;
                      crawl.status = 'completed';    
                      crawl.completedOn = new Date();
                      crawl.log = [stdout];
                        
                    
                        
                      data.links.forEach(function(item){
                            if(crawl.url == item.url) return;
                            var pg = new Page({
                                                        testid: crawl.testid,
                                                        userid: crawl.userid,
                                                        url: item.url,                                                                                
                                                        html: item.html,
                                                        httpStatus: item.status,
                                                        status: 'created',
                                                        created:new Date()                    
                                                    });  

                            pg.save(function(err,page){
                                            if(err)
                                                return console.log("Error creating the page",err);
                                            console.log("page created",page.url );
                                            
                                            console.log("creating tasks for url:",page.url);
                                
                                            test.addlog(20003,"Page created url:" + page.url,{pageid:page._id},function(err,task){});
                                
                                            bluep.pageMinions.map(function(item){
                                                                    var tsk = new Task({
                                                                                        testid: test._id,
                                                                                        userid: test.userid,
                                                                                        pageid: page._id,
                                                                                        created: new Date(),
                                                                                        minion: item.slug,
                                                                                        size: item.size,
                                                                                        config: item.config?item.config:{},//by now
                                                                                        status: 'created'
                                                                                        });
                                                                    tsk.save(function(err,task){                        
                                                                        //console.log("new task [",item.slug,"] for url:",page.url);
                                                                    });                    
                                                                });     
                                            

                                        });                  



                      });
                       crawl.save(function(err,crl){
                            if(err){
                                return console.log("Error saving the completed crawl"); 
                            }
                            test.addlog(20010,"Crawl Completed",{crawlid:crawl._id},function(err,task){});
                            return console.log("Crawl completed saved");                           
                       });    
                    
                    });

                }
                console.log("done");

            });




            
        });
    
}