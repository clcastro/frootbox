var path = require('path'),
    express = require('express'),
    http = require('http'),
    mongoose = require('mongoose'),    
    User = require('../webroot/models/user');
    
var FROOTCONFIG = require('config');

var app = express();

// Configuration
app.set('views', __dirname + '/views');
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');

//app.use(express.logger());
app.use(express.bodyParser());
app.use(express.methodOverride());

app.use(express.cookieParser());
app.use(express.session({secret: 'loremipsumpassworddolorsitamet' }));

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.configure('development', function(){
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.locals.pretty = true;
});

app.configure('production', function(){
    app.use(express.errorHandler());
    app.locals.pretty = true;
});

// Connect mongoose
mongoose.connect(FROOTCONFIG.db.mongo);

// Setup routes
require('./routes')(app);

http.createServer(app).listen(FROOTCONFIG.general.port, '0.0.0.0', function() {
    console.log("Express server listening on %s:%d in %s mode", '0.0.0.0', FROOTCONFIG.general.port, app.settings.env);
    //log.alert("[app-restart] 0.0.0.0:",FROOTCONFIG.general.port," in ",app.settings.env," mode");
    
     var t = new User();
    t.save(function(e,ttt){
        console.log("saved");
        console.log(e,ttt);
    });    
    
});

