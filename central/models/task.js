var mongoose = require('mongoose'),
    AccountPlan = require('./accountplan'),
    User = require('./user'),    
    Page = require('./page'),
    Test = require('./test'),
    Taskify = require('./taskify'),
    Schema = mongoose.Schema;

var TaskSchema = new Schema({    
    testid : { type: Schema.ObjectId, ref: 'Test' },
    userid : { type: Schema.ObjectId, ref: 'User' },
    pageid : { type: Schema.ObjectId, ref: 'Page' },
    created: Date,
    completedOn: Date,
    status: String,    
    minion: String,
    size: String,
    config:Object    
});

TaskSchema.plugin(Taskify);

module.exports = mongoose.model('Task', TaskSchema);