var mongoose = require('mongoose'),    
    Schema = mongoose.Schema;

var MinionSchema = new Schema({            
    isPageMinion:{type:Boolean,default:true},
    isSiteMinion:{type:Boolean,default:false},
    name: String,
    slug: String,
    created: Date,    
    lastModified: Date,
    url:String,
    defaultConfig:Array,
    resultRequiredScripts:Array,
    resultHtml:String,
    resultJS: String,
    resultDefaultSize:{type:String,default:"2x2"}
});

module.exports = mongoose.model('Minion', MinionSchema);