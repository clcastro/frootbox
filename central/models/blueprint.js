var mongoose = require('mongoose'),  
    Minion = require('./minion'),
    Schema = mongoose.Schema;

var BluePrintSchema = new Schema({            
    created:Date,
    modified: Date,
    name: {type: String,default:""},
    slug: {type: String,default:""},
    siteMinions:Array,
    pageMinions:Array,
    homeMinions: Array,
    maxPages: {type:Number,default:10},
    timeoutSeconds: {type:Number,default:300}    
});

module.exports = mongoose.model('BluePrint', BluePrintSchema);