var mongoose = require('mongoose'),
    AccountPlan = require('./accountplan'),
    User = require('./user'),    
    Page = require('./page'),
    Test = require('./test'),
    Taskify = require('./taskify'),
    Schema = mongoose.Schema;

var CrawlSchema = new Schema({    
    testid : { type: Schema.ObjectId, ref: 'Test' },
    userid : { type: Schema.ObjectId, ref: 'User' },    
    foundPages:Array,
    created: Date,
    completedOn: Date,
    status: String,
    url: String,
    limit:Number,
    log: Array,
    config:Object    
});

CrawlSchema.plugin(Taskify);

module.exports = mongoose.model('Crawl', CrawlSchema);