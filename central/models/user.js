var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose');

var Plan = require('./plan'),
    Account = require('./account'),
    AccountPlan = require('./accountplan'),
    UserAccount = require('./useraccount'),
    Test = require('./test');


var UserSchema = new Schema({    
    openid: String,
    fbid: String,
    githubId:String,
    twitterUsername: String,
    githubUsername: String,
    email: String,
    birthdate: Date,
    displayName : String,    
    name: { familyName :String, givenName :String, middleName :String},
    photos : [{value:String}],
    created:Date,
    lastLogin:Date,
    emailValidated: {type:Boolean,default:false},
    fbMeta:Object,
    githubMeta:Object,
    isGuestUser: {type:Boolean,default:false}
});

UserSchema.plugin(passportLocalMongoose,{usernameField:'email'});

UserSchema.methods.asignBasicPlanWithNewUser = function (cb) {
    var theuser = this;
    
   Plan.findOne({slug: "basic" }, function(err,basicplan){    
       if(err){ console.log("errorplan",err); return cb(err,null); }
       
        var account = Account({ 
                            created: new Date(),
                            users: [theuser],
                            plans:[basicplan]
                        });            
        account.save(function(err,theacc){          
            if(err){ console.log("erroraccount",err.stack); return cb(err,null); }
            
            var uaccount = UserAccount({ 
                created: new Date(),
                userid: theuser,
                accountid: theacc,
                acl:"admin"
            });            
            uaccount.save(function(err,theuacc){                
                if(err){ console.log("erroruaccount",err); return cb(err,null); }

                var accplan = AccountPlan({ 
                            created: new Date(),
                            accountid: theacc,
                            planid: basicplan,
                            hooks: [],
                            programmedTasks:[]
                        });            
                accplan.save(function(err,theaccplan){   
                    if(err){ console.log("erroraccplan",err); return cb(err,null); }
                    
                    cb(null,theuacc);
                });                                                    

            });
        });       
   });
           
                                    
                                                        
                                        

                                                
                                                
        
    
    
}

module.exports = mongoose.model('User', UserSchema);