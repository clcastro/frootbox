var mongoose = require('mongoose'),
    AccountPlan = require('./accountplan'),
    User = require('./user'),
    UserBluePrint = require('./userblueprint'),
    BluePrint = require('./blueprint'),
    Taskify = require('./taskify'),
    Schema = mongoose.Schema;

var TestSchema = new Schema({    
    accountplanid : { type: Schema.ObjectId, ref: 'AccountPlan' },
    userid : { type: Schema.ObjectId, ref: 'User' },
    bluePrintId : { type: Schema.ObjectId, ref: 'BluePrint' },
    userBluePrintId : { type: Schema.ObjectId, ref: 'UserBluePrint' },
    created: Date,
    completedOn: Date,
    pages:Array,
    status: String,
    callUrl: String, 
    finalUrl: String,
    homeContentType: String,
    homeHeaders: Array,
    homeHtml: String,
    homeBigImageUrl: String 
});

TestSchema.plugin(Taskify);

module.exports = mongoose.model('Test', TestSchema);