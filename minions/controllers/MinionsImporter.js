var minionDefinitions = {};
var minionControllers = {};

var minionPath = './lib/minions/';

function throw404(res){
	res.writeHead(404, {
		"Content-Type": "text/plain"
	});
	res.write("404 Not Found\n");
	res.end();
	return;
}

exports.loadMinions = function()
{
	var fs = require('fs');
	var files = fs.readdirSync(minionPath);
	for (var i in files) {
		var filename = files[i];
		if (filename.indexOf('.js') > 0 && filename.indexOf('-visual') <= 0) {
			var moduleName = filename.slice(0,-3)
			var minion = require('../' + minionPath + filename);

			minionDefinitions[moduleName] = minion.definition();
			minionControllers[moduleName] = minion;
		}
	}
}

exports.knownMinions = function()
{
	var defs = [];
	for(var key in minionDefinitions) {
		var minion = minionDefinitions[key];
		minion['id'] = key;
		defs.push(minion);
	}
	return defs;
}

exports.listKnownMinions = function(req, res)
{
	res.setHeader('Content-Type', 'application/json');
	res.end(JSON.stringify(minionDefinitions));
}

exports.execute = function(req, res)
{
	var minionId = req.params.id;
	var jobId = req.params.jobId;
	var args = req.body;

	var minionType = args.type;
	if (!minionType) {
		minionType = 'page';
	}

	var minion = null;
	var minionDef = null;
	if (minionId) {
		minion = minionControllers[minionId];
		minionDef = minion.definition();
	}

	if (!minionId || !minion) {
		console.error("Missing minion id");
		return throw404(res);
	}

	if (minionType == 'page' && !minionDef.canBePageMinion) {
		console.error("Cannot be page minion (Bad argument)");
		return throw404(res);
	}
	if (minionType != 'page' && !minionDef.canBeSiteMinion) {
		console.error("Cannot be site minion (Bad argument)");
		return throw404(res);
	}

	var url = args.url;
	if (url && !minionDef.acceptsUrl) {
		console.error("Minion does not accept URL (Bad argument)");
		return throw404(res);
	}
	if (!url && minionDef.requiresUrl) {
		console.error("Minion requires URL (Missing argument)");
		return throw404(res);
	}

	var html = args.html;
	if (html && !minionDef.acceptsHtml) {
		console.error("Minion does not accept HTML (Bad argument)");
		return throw404(res);
	}
	if (!html && minionDef.requiresHtml) {
		console.error("Minion requires HTML (Missing argument)");
		return throw404(res);
	}

	var headers = args.headers;
	if (!headers && minionDef.requiresHeaders) {
		console.error("Minion requires headers (Missing argument)");
		return throw404(res);
	}

	var minionResult = minion.execute(args, function(minionResult) {
		var result = {
			'minion':minionDef,
			'job':jobId,
			'data':minionResult
		}
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(result));
	});
}
