
var MinionSchema = new Schema({
    name: String,
    slug: String,
    url:String,
    canBePageMinion:{type:Boolean,default:true},
    canBeSiteMinion:{type:Boolean,default:false},
    requireHtml:{type:Boolean,default:false},
    requireUrl:{type:Boolean,default:false},
    requireHeaders:{type:Boolean,default:false},
    resultDefaultSize:{type:String,default:"2x2"},
    availableSizes:Array,
    created: Date,
    lastModified: Date
});

module.exports = mongoose.model('Minion', MinionSchema);
