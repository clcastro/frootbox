var path = require('path'),
    express = require('express.io');

var FROOTCONFIG = require('config');
var logentries = require('node-logentries');

var log = logentries.logger({ token:FROOTCONFIG.log.token });

var app = express();

app.http().io()

// Setup the ready route, and emit talk event.
app.io.route('ready', function(req) {
    req.io.emit('talk', {
        message: 'io event from an io route on the server'
    })
})


// Configuration
app.set('views', __dirname + '/views');
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');

//app.use(express.logger());
app.use(express.bodyParser());
app.use(express.methodOverride());

app.use(express.cookieParser());
app.use(express.session({secret: 'loremipsumpassworddolorsitamet' }));

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.configure('development', function(){
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.locals.pretty = true;
});

app.configure('production', function(){
    app.use(express.errorHandler());
    app.locals.pretty = true;
});

// Setup routes
require('./routes')(app);
app.listen(FROOTCONFIG.general.port);
