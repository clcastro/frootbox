
exports.definition = function()
{
	var def = {
		name: 'Chilean',
		description:'This is a test Minion. It will respond if the URL is Chilean or not.',
		slug: 'chile',
		canBePageMinion:true,
		canBeSiteMinion:false,
		acceptsHtml:false,
		acceptsUrl:true,
		requiresHtml:false,
		requiresUrl:true,
		requiresHeaders:false,
		resultDefaultSize:"1x1",
		availableSizes:["1x1"],
	}
	return def;
}

exports.execute = function(minionArgs, callback)
{
	var content;
	if (minionArgs.url && minionArgs.url.length > 0) {
		if (minionArgs.url.indexOf('.cl') > 0) {
			callback('Chilean!');
			return;
		} else {
			callback('Foreign!');
			return;
		}
	}
	callback(null);
	return;
}
