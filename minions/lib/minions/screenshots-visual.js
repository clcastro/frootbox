 return {
   init:function(view){
      var data = view.model.get("data").data;
     $('#screenshots').empty();
     data.forEach( function(ss) {
       var str = "<a data-gallery href='" + ss.url + "'>";
       str += "<div class='ss_thumb' style='background-image:url(" + ss.url +");'>";
       str += ss.description;
       str += "<div></a>";
       $('#screenshots').append(str);
     });

   },
   render:function(view){
      console.log("render");
   },
   update:function(view){
      console.log("update");
   },
   resize:function(view){
      console.log("resize");
   },
   remove:function(view){
      console.log("remove");
   }
 };
