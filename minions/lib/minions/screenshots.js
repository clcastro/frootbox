var AWS = require('aws-sdk'),
phantom = require('phantom'),
imagemagick = require('imagemagick-native'),
fs = require('fs');

AWS.config.loadFromPath('../webroot/config/config-aws.json');
var bucket = FROOTCONFIG.aws.bucket;

exports.definition = function()
{
	var def = {
		name: 'Screenshot',
		description:'Take screenshots in different resolutions.',
		slug: 'screenshot',
		canBePageMinion:true,
		canBeSiteMinion:false,
		acceptsHtml:false,
		acceptsUrl:true,
		requiresHtml:false,
		requiresUrl:true,
		requiresHeaders:false,
		resultDefaultSize:"1x1",
		availableSizes:["1x1"],
	}
	return def;
}

exports.execute = function(minionArgs, callback)
{
	var url = minionArgs.url;

	var jobs = new Array();
	var jobs_done = new Array();

	var platform_job1 = new Object();
	platform_job1.description = 'iPhone 4 Portrait';
	platform_job1.width = 320;
	platform_job1.height = 480;
	platform_job1.userAgent = 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 6_0 like Mac OS X; en-us) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8356.25';

	jobs.push(platform_job1);

	var platform_job2 = new Object();
	platform_job2.description = 'iPhone 4 Landscape';
	platform_job2.width = 480;
	platform_job2.height = 320;
	platform_job2.userAgent = 'Mozilla/5.0 (iPhone; U; CPU iPhone OS 6_0 like Mac OS X; en-us) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A523 Safari/8356.25';

	jobs.push(platform_job2);

	var error = function(reason) {
		console.error(reason);
	}

	var screenshot_success = function(platform_job) {
		upload_local_image(platform_job, upload_success);
	}

	var upload_success = function(platform_job) {
		console.log("> upload_success (" + platform_job.url + ")");

		var result = platform_job;
		delete result.path;
		jobs_done.push(result);

		if (jobs.length == jobs_done.length) {
			callback(jobs_done);
		}
	}

	for (var idx in jobs) {
		fetch_screenshot(url, jobs[idx], screenshot_success, error);
	}
}

function fetch_screenshot(url, platform_job, successCallback, errorCallback)
{
	var resources = {};
	var documentStatus = 0;
	phantom.create(function (ph) {
		ph.createPage(function (page) {
			page.set('settings.userAgent', platform_job.userAgent, function (res) {
				console.log(">> user agent set", platform_job.description, url, res);
				page.set('onResourceReceived', function (res) {
					resources[res.url] = res;
				});

				console.log(">> phantom opening url", platform_job.description, url);
				page.open(url, function (status) {
					if (status !== 'success') {
						console.log('Unable to access network');
					} else {
						page.get("content",function(content){
							page.evaluate(function () { return document.documentURI; },function (result) {
								documentStatus = resources[result].status ;
								if(documentStatus == 200) {
									var now = new Date();
									var imageID = Math.floor(Math.random() * 10) + parseInt(now.getTime()).toString(36).toUpperCase()
									var imagePath = "./temp/"+imageID+".jpg";
									page.set('viewportSize', {width:platform_job.width, height: platform_job.height}, function(){

										page.set('clipRect', { top: 0, left: 0, width:platform_job.width, height: platform_job.height }, function(){
											page.render(imagePath, { format: "jpg", quality: 80 },function(){
												ph.exit();
												platform_job.path = imagePath;
												successCallback(platform_job);
											});
										});
									});
								} else {
									ph.exit();
									errorCallback('Error');
								}
							});
						});
					}
				});
			});
		});
	});
}

function upload_local_image(platform_job, callback) {
	console.log('upload_local_image');
	try
	{
		var now = new Date();
		var imageID = Math.floor(Math.random() * 10) + parseInt(now.getTime()).toString(36).toUpperCase();
		var srcData  = fs.readFileSync(platform_job.path);
		var fileStats =  fs.statSync(platform_job.path)
		var screenshotData =  imagemagick.identify({srcData: srcData });

		var filekey = imageID+".jpg";

		var s3 = new AWS.S3();
		var s3_req = {
			ACL: 'public-read',
			Bucket: bucket,
			Key: filekey,
			Body: srcData,
			ContentType: 'image/jpeg'
		};

		console.log('putObject');
		s3.putObject(s3_req, function (err1, response) {
			console.log(bucket,filekey);
			console.log(arguments);
			if(err1){
				return console.error("Error saving the image",err1);
			}

			fs.unlink(platform_job.path, function(err) {
				if (err) {
					return console.log("Error removing the image");
				}
			});
			platform_job.url = "https://s3-us-west-2.amazonaws.com/"+bucket+"/"+filekey;

			callback(platform_job);
		});
	}
	catch(e){
		console.log("exec-read-error",e);
	}
}
