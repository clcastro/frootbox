var http = require('http');
var https = require('https');
exports.definition = function()
{
	var def = {
		name: 'CSS Validator',
		description:'It will respond the response from w3c CSS validator',
		slug: 'cssvalidator',
		canBePageMinion:true,
		canBeSiteMinion:true,
		acceptsHtml:false,
		acceptsUrl:true,
		requiresHtml:false,
		requiresUrl:true,
		requiresHeaders:false,
		resultDefaultSize:"1x1",
		availableSizes:["1x1"],
	}
	return def;
}

exports.execute = function(minionArgs, callback)
{	
    var headers = { 
	    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0'
	};    
    
    http.get({
        host: 'jigsaw.w3.org', 
        path: '/css-validator/validator?output=json&uri=' + encodeURIComponent(minionArgs.url),
        headers:headers

       }, function(res) {

          var str = "";
          res.on('data', function(d) {
            str+= d.toString();
          });
          res.on('end',function(){	      	
            try{
                var result = JSON.parse(str);	      		
            }
            catch(e){
                callback({error:true,msg:e});
            }

            if(result.cssvalidation)
            {
                var obj = {                
                    csslevel: result.cssvalidation.csslevel,
                    errorCount: result.cssvalidation.result.errorcount,
                    warningCount: result.cssvalidation.result.warningcount,
                    valid: result.cssvalidation.validity
                };
                callback(obj);                
            } 
            else
            {
                callback({error:true,msg:"Error parsing the response"});
            }            

          });
    }).on('error', function(e) {
        callback({error:true,msg:e});
    });	    

    
}
