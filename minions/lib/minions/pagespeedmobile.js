var https = require('https');
exports.definition = function()
{
	var def = {
		name: 'PageSpeed Mobile',
		description:'This is a test Minion. It will respond the response from google page speed web mobile version',
		slug: 'pagespeedmobile',
		canBePageMinion:true,
		canBeSiteMinion:true,
		acceptsHtml:false,
		acceptsUrl:true,
		requiresHtml:false,
		requiresUrl:true,
		requiresHeaders:false,
		resultDefaultSize:"1x1",
		availableSizes:["1x1"],
	}
	return def;
}

exports.execute = function(minionArgs, callback)
{	
    https.get({
        host: 'www.googleapis.com', 
        path: '/pagespeedonline/v1/runPagespeed?url=' + encodeURIComponent(minionArgs.url) + 
              '&key=AIzaSyBNskxT8SAXwaBXslwLZFpstzTB8mNOZB0&strategy='+'mobile'
        }, function(res) {

          var str = "";
          res.on('data', function(d) {
            str+= d.toString();
          });
          res.on('end',function(){
            try{
                var pagespeed = JSON.parse(str);
            }
            catch(e){		      		
                callback && callback({error:true,msg:e},null);
                return;
            }

            if(pagespeed.pageStats){
                return callback && callback(pagespeed);
            }

            return callback && callback({error:true,msg:"Unknown Error"},null);
          });
    }).on('error', function(e) {
        return callback && callback({error:true,msg:e},null);		    
    });	        

    

    
}
