var http = require('http');
var https = require('https');
exports.definition = function()
{
	var def = {
		name: 'HTML Validator',
		description:'It will respond the response from w3c HTML validator',
		slug: 'htmlvalidator',
		canBePageMinion:true,
		canBeSiteMinion:true,
		acceptsHtml:false,
		acceptsUrl:true,
		requiresHtml:false,
		requiresUrl:true,
		requiresHeaders:false,
		resultDefaultSize:"1x1",
		availableSizes:["1x1"],
	}
	return def;
}

exports.execute = function(minionArgs, callback)
{	
    var headers = { 
	    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:24.0) Gecko/20100101 Firefox/24.0'
	};    
    
    http.get({
			    host: 'validator.w3.org', 
			    path: '/check?output=json&uri=' + encodeURIComponent(minionArgs.url),
			    headers:headers
			    }, 
         function(res) {
          var str = "";
          res.on('data', function(d) {
            str+= d.toString();
          });
          res.on('end',function(){	      	
            try{
                var result = JSON.parse(str);	      		
            }
            catch(e){
                callback({error:true,msg:e});
            }
            callback(result);

          });
    }).on('error', function(e) {
        callback({error:true,msg:e});
    });	    

    
}
