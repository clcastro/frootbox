
exports.definition = function()
{
	var def = {
		name: 'URL length',
		description:'This is a test Minion. It will respond the lenght of the URL or the HTML, if provided.',
		slug: 'length',
		canBePageMinion:true,
		canBeSiteMinion:false,
		acceptsHtml:true,
		acceptsUrl:true,
		requireHtml:false,
		requireUrl:false,
		requireHeaders:false,
		resultDefaultSize:"1x1",
		availableSizes:["1x1"],
	}
	return def;
}

exports.execute = function(minionArgs, callback)
{
	var content;
	if (minionArgs.url && minionArgs.url.length > 0) {
		callback(minionArgs.url.length);
		return;
	}
	if (minionArgs.html && minionArgs.html.length > 0) {
		callback(minionArgs.url.length);
		return;
	}
	callback(null);
	return;
}
