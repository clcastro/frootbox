var https = require('follow-redirects').https;
$ = require('jquery'),
url = require('url'),
validator = require('validator'),
FROOTCONFIG = require('config');

//Controllers
var MinionsImporter = require('./controllers/MinionsImporter');

MinionsImporter.loadMinions();

module.exports = function (app) {
    app.get('/', function (req, res) {
        var minions = MinionsImporter.knownMinions();
        res.render("usage", {'minions':minions});
    });
    app.get('/minion/list',MinionsImporter.listKnownMinions);
    app.post('/minion/:id/execute/:jobId',MinionsImporter.execute);
};
