#!/bin/sh

brew update
brew install pkg-config
brew install node
brew install mongo
brew install phantomjs
brew install imagemagick

cd webroot; npm install;
cd ../
cd minions; npm install;
cd ../
cd central; npm install;
cd ../
