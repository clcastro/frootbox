var Minion = require('../models/minion');
var BluePrint = require('../models/blueprint');
var escape = require('escape-html');


exports.JsAllMinions = function(req, res)
{   
    Minion.find(function (err, minions) {
       if (err) return console.error(err);                         
        var scripts = [];        
        var jst = [];
        minions.forEach(function(min){
            scripts.push("{name:'"+min.name+"',slug:'"+min.slug+"',gen: function(){"+min.resultJS+"},defaultSize: '"+min.resultDefaultSize+"' }");
            jst.push('JST["minions/'+min.slug+'"] = \''+min.resultHtml.replace(/(\r\n|\n|\r)/gm,"")+'\';');
        });        
        
        res.setHeader('Content-Type', "text/javascript");        
        var str = "var BootstrapDataMinions =  ["+scripts.join(",")+"]; \n var JST = {}; "+jst.join("\n")+" \n ";
        BluePrint.findOne({slug:"default-guest"},function(err,blue){
            if(err){  return res.end(str);   }
            if(blue){
                var bluestr =  JSON.stringify(blue);
                str+= "var BootstrapDataBlueprint = "+bluestr+";";
            }
            return res.end(str);  
        });    
    });      
} 


exports.CssAllMinions = function(req, res)
{   
    Minion.find(function (err, minions) {
       if (err) return console.error(err);                         
        var scripts = [];        
        minions.forEach(function(min){
            scripts.push(min.resultCSS);
        });        
        res.setHeader('Content-Type', "text/css");
        res.end(scripts.join("\n"));        
    })    
}

