var Minion = require('../models/minion');

exports.viewAllMinions  = function(req,res){
    
    Minion.find(function (err, minions) {
       if (err) return console.error(err);                 
        res.render('minions-all', { bodyclass:'page-view-minions',user:req.user, minions:minions });  
    })    
}

exports.addMinion       = function(req,res){    
    res.render('minions-add', { bodyclass:'page-add-minion',user:req.user });    

}

exports.saveNewMinion   = function(req,res){
        var minion = new Minion({
                        name:req.body.name,
                        slug:req.body.slug,
                        url:req.body.url,
                        isPageMinion: req.body.isPageMinion?true:false,
                        isSiteMinion: req.body.isSiteMinion?true:false,
                        resultDefaultSize:req.body.size,
                        availableSizes: req.body.sizes,
                        resultHtml : req.body.resultHtml,
                        resultCSS : req.body.resultCSS,
                        resultJS : req.body.resultJS,
                        scripts:  req.body.scripts,
                        css:   req.body.css,
                        defaultConfig: req.body.config,
                        examplePageResponse: req.body.pageresponse,
                        examplePageResponse: req.body.siteresponse,
                        created: new Date(),
                        lastModified: new Date()
                    });
    
    minion.save(function(err,mn){
        if(err)
        {
            res.render('minions-add', { bodyclass:'page-add-minion',user:req.user });
        }
        else{
            res.redirect('/dev/leminions');
        }
    });
        
        
    
    
}

exports.editMinion      = function(req,res){
    res.render('minions-update', { bodyclass:'page-update-minion',isupdateminion:true,user:req.user });
}

exports.updateMinion    = function(req,res){
    Minion.findOne({_id:req.params.id} ,function (err, minion) {        
       if (err) return res.json({status:"error",error:err});
        
        minion.resultHtml = req.body.resultHtml;
        minion.resultCSS = req.body.resultCSS;
        minion.resultJS = req.body.resultJS;
        minion.examplePageResponse = req.body.examplePageResponse;
        minion.exampleSiteResponse = req.body.exampleSiteResponse;
        minion.resultDefaultSize = req.body.size;
        minion.lastModified = new Date();
        minion.save(function(e,mn){
            if (e) return res.json({status:"error",error:e});
            res.json({status:'ok',minion: mn});     
        });                
    });
    
}


exports.testMinion      = function(req,res){ 
    Minion.findOne({_id:req.params.id} ,function (err, minion) {
        console.log("LE Minion");
        console.log(minion);
       if (err) return console.error(err);                 
        res.render('minions-test', { bodyclass:'page-test-minion',isupdateminion:true,user:req.user,minion:minion });
    })      
}

exports.updateTestMinion    = function(req,res){

}