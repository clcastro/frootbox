var validator = require('validator');
var nodemailer = require("nodemailer");

var mail = require("nodemailer").mail;

function createMailer()
{
    return nodemailer.createTransport("SMTP",{
        service: "Gmail",
        auth: {
            user: "frootbox.contact@gmail.com",
            pass: "frootbox123"
        }
    });
}



exports.viewContactForm = function(req, res)
{
    res.render('contact', { user:req.user, script:'contact' });
}

exports.sendContactRequest = function(req, res)
{
    var email = req.body.email;
    var name = req.body.name;
    var phone = req.body.phone;
    var message = req.body.message;

    var body = name + '(' + email +') wrote:\n\n' + message;

    var mailOptions = {
        from: "FrootBox Contact ✔ <frootbox.contact@gmail.com>", // sender address
        to: "Felipe Baytelman <felipe.baytelman@gmail.com>, Claudio Castro <yayolius@gmail.com>", // list of receivers
        subject: "Message from Contact Form", // Subject line
        text: body, // plaintext body
    }

    var mailer = createMailer();
    mailer.sendMail(mailOptions, function(error, res){
        mailer.close();
        if(error){
            console.log(error);
        }else{
            console.log("Message sent: " + response.message);
        }

        res.render('contact_thanks', {layout: false});
    });
}
