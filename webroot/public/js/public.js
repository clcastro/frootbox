var frooTest = {};
(function () {
    
    if($("body").is(".page-home"))
    {
       // io = io.connect()

        // Emit ready event.
        //io.emit('ready') 

        // Listen for the talk event.
        //io.on('talk', function(data) {
        //            console.log(data.message)
        //});
        /***
        io.on("testupdate",function(data){
            $form = $(".da-check");
            switch(data.status){
                case "error":
                      $form.find("input").removeAttr("disabled");
                      $form.find(".btn").val("Check");
                      alert("Error creating the test");  
                break;
                case "success":
                    $("body").addClass("loading");
                    $form.find("input").removeAttr("disabled").blur();
                    $form.find("input").on("focus",function(){ $(this).blur(); });
                    needToConfirm = true;    
                    frooTest.requestid = data.requestid;
                break;
                case "active":
                    console.log("starting to process the request");
                break;
                case "failded-creating":
                    $form.find("input").removeAttr("disabled");
                    $form.find(".btn").val("Check");
                    frooTest.statusCode = data.status;
                    alert("Error creating the test: [status code:" +data.status+"]" ); 
                break;                    
                case "test-created":
                    frooTest.statusCode = data.status;
                    frooTest.id = data.testid;
                    console.log("The test was created");    
                break;
                    
                case "test-data":
                    console.log("receiving data",data.data.homeBigImageUrl);
                    
                    $("#bghome").hide();
                    $("<img />").load(function(){
                        $("#bghome .in").css({"background-image":"url('"+data.data.homeBigImageUrl+"')"});
                        $("#bghome").fadeIn("slow");
                    }).attr("src",data.data.homeBigImageUrl);
                break;
                
            }
            
            
        });
        /****
        $(".da-check").on("submit",function(ev){
            ev.preventDefault();
            var url = $(this).find("input[type=text]").val();
            if( url == "" ) return alert("Url cannot be empty");
            else if( !ValidUrl(url)) return alert("The url you entered is not valid");
            else{
                $(this).find("input").attr("disabled","disabled");
                $(this).find(".btn").val("Validating");
                $form = $(this); 
                
                var w = window.innerWidth?window.innerWidth: window.width;
                var h = window.innerHeight?window.innerHeight: window.height;
                
                io.emit('test',{url:url,w: w, h:h});                                             
            }
            
        });
        ****/

        
    }    
    if($("#signup-form_id").length){
        $("#signup-form_id").validate({ focusInvalid: true});
        // Validate username
        $("#email").rules("add", {required: true,email: true });
        $("#email2").rules("add", {required: true,email: true,equalTo:"#email"});
        
        $("#password").rules("add",{ required: true, minlength: 6});
        $("#password2").rules("add",{ required: true, minlength: 6,equalTo:"#password"});
    }
    if( $("#signin-form_id").length ){
        $("#signin-form_id").validate({ focusInvalid: true });

        // Validate username
        $("#email").rules("add", {
            required: true,
            minlength: 3
        });

        // Validate password
        $("#password").rules("add", {
            required: true,
            minlength: 6
        });
    }
})();

function ValidUrl(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  if(!pattern.test(str)) {
    return false;
  } else {
    return true;
  }
}

 var needToConfirm = false;

window.onbeforeunload = confirmExit;
function confirmExit()
{
if (needToConfirm)
  return "You are currently loading a check, if you exit you could maybe not be able to recover it, are you sure you wan't to exit?";
}