$(function() {
  // Set up some options for jQuery and plugins.
  
  Backbone.emulateJSON = true;
    
  // Initialize Backbone views.
  //App.testView = new App.Views.Test({ el: $("body") });
  //App.router = new App.Router;

  // Initialize the Backbone router.
  //Backbone.history.start();

  //We first check if we have to load a test
  App.initialize( { testid: $("#bootstrapdata #testid").val() } ); 
  
    
  
});




/*****************************************************
?======================= Development =================?

1. Primero detectamos si tenemos un id de test (cargado como ruta o como input hidden), si no lo tenemos asumimos que el test es nuevo.
//Todo esto es asumiendo un test nuevo
2. Mostramos la barra de llamado a crear un test, y conectamos los eventos .
3. Si estamos como guest, cargamos el blueprint y los minions asociados al test guest, además de los css y html asociados a cada uno de los minions. Mas adelante desarrollamos los blueprint personalizados, ahora solo el guest
4. Al enviar el form de test creamos un testrequest y:
    4.a - Validamos y enviamos el form
    4.b - si es erroneo (digase encontramos un codigo http distinto de 2xx) mostramos el mensaje de error y cancelamos el test de forma visual.
    4.c - si es exitoso, crea el test y empieza a mandar actualizaciones del avance pasando al paso 5
5. Creamos el log de eventos que le aparecera al usuario, mostrando el avance.
6. Cargamos los datos del test y creamos un nuevo modelo del test.
7. Una vez se ha creado al pagina de la portada, inicializamos la muestra de minionboxes para la portada y para el sitio
8. Vamos actualizando el estado de avance de cada una de las tasks para la portada y a medida que van completandose cargamos los resultados de las tareas de la portada en su minionbox correspondiente.
9. Hacemos lo mismo que el paso 8 para los siteminions.
10. una vez completado el test, mostramos la lista de páginas para poder acceder a cada una de ellas, mas adelante esta lista de paginas aparecera cuando tengamos listo el crawling y mostraremos sus actualizaciones tal cual lo hariamos con la portada.

****************************************************/