$(document).ready(function()
{
    $("#contact").submit(function(event) {

        event.preventDefault();

        var url = "contact";

        $.ajax({
            type: "POST",
            url: url,
            data: $("#contact").serialize(),
            success: function(data)
            {
                $("#contact_container").html(data);
            }
        });
    });
});
