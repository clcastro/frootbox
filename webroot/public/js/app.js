'use strict';
window.App = {};

App.config = {
    needToConfirm: false
}

App.helpers = {
    ValidUrl: function(str) {
      var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
      if(!pattern.test(str)) {
        return false;
      } else {
        return true;
      }
    },
    confirmOnExit: function(){
        if(App.config.needToConfirm) return "You are currently loading a check, if you exit you could maybe not be able to recover it, are you sure you wan't to exit?";
    }
}

window.onbeforeunload =  App.helpers.confirmOnExit;


App.Models = {};
App.Collections = {};
App.Views = {};


App.Models.User     =  Backbone.Model.extend({});
App.Models.Minion   =  Backbone.Model.extend({
                             defaults: {
                                size:  "1x1",
                                name: "Minion",
                                slug: "minion",                    
                                isPageMinion: true,
                                isSiteMinion: false,                    
                                defaultConfig: {},                    
                                actions: null,
                                availableSizes: ["1x1","2x1","3x1","4x1","2x2","3x2","4x2"],
                                data:{},
                                ids:{pageid:"a1234",siteid:"a1234"}
                              },                                    
                              changeDefaultSize: function() {            
                              }                 
                        });

App.Models.BluePrint =  Backbone.Model.extend({ 
                            default:{

                            },  
                            initialize:function(){ 
                    
                            }
                        });
App.Models.Page      =  Backbone.Model.extend({ 
                             initialize:function(){
                                console.log("init:page");
                                 this.on('change', this.change, this);
                                 
                             },
                            change:function(){
                                console.log("change:page");
                            }

                        });
App.Models.Test      =  Backbone.Model.extend({  });


App.Collections.Minions =  Backbone.Collection.extend({
                            model: App.Models.Minion
                           });

App.Collections.Pages =  Backbone.Collection.extend({
                            model: App.Models.Page
                           });



App.Views.Test  = Backbone.View.extend({  
        initialize:function(){
            console.log("test:initialize");
            if(!this.model.get("_id")){
                this.$el.find(".da-check").fadeIn();
            }
            
            App.io.on("testupdate",$.proxy(this.testUpdate,this));
            this.render();            
        },     
        events:{
            "submit .da-check" : "sendTestRequest",
        },
        testUpdate: function(data){
            var $form = this.$el.find(".da-check");
            var self = this;
            console.log("testUpdate",data.status);
            switch(data.status){
                case "error":
                      $form.find("input").removeAttr("disabled");
                      $form.find(".btn").val("Check");
                      alert("Error creating the test");  
                    App.config.needToConfirm = false;    
                break;
                case "success":                    
                    $("body").addClass("loading");
                    $form.find("input").removeAttr("disabled").blur();
                    $form.find("input").on("focus",function(){ $(this).blur(); });
                    App.config.needToConfirm = true;    
                    this.log("validating-url","Validating URL",false);
                break;
                case "active":
                    console.log("starting to process the request");
                break;
                case "failded-creating":
                    $form.find("input").removeAttr("disabled");
                    $form.find(".btn").val("Check");
                    //frooTest.statusCode = data.status;
                    App.config.needToConfirm = false;    
                    alert("Error creating the test: [status code:" +data.status+"]" ); 
                break;                    
                case "test-created":
                    //frooTest.statusCode = data.status;
                    //frooTest.id = data.testid;
                    this.log("validating-url","",true);
                    this.log("home-screen","Taking initial screenshots",false);
                    console.log("The test was created");    
                break;
                    
                case "test-data":
                    console.log("receiving data",data.data.homeBigImageUrl);                    
                    this.log("home-screen","",true);
                    this.log("home-test","Starting Home Tests",false);
                    $("#bghome").hide();
                    $("<img />").load(function(){
                        $("#bghome .in").css({"background-image":"url('"+data.data.homeBigImageUrl+"')"});
                        $("#bghome").fadeIn("slow");
                    }).attr("src",data.data.homeBigImageUrl);
                break;
                
                case "page-update":
                    
                    
                    if(!this.model.get("pages") || this.model.get("pages").length == 0)
                    {
                        data.page.id = data.page._id;
                        var page =  new App.Models.Page(data.page);                        
                        page.set("isHome",true);
                        var pages = new App.Collections.Pages([page]);
                        this.model.set("pages",pages);                        
                        
                        page.set("minionsresult",new App.Collections.Minions());
                        this.model.get("blueprint").get("homeMinions").each(function(minion){                      
                          page.get("minionsresult").add(minion.clone());
                        });
                        this.renderResult();
                        
                        _.each(data.page.minions,function(min){                            
                             page.get("minionsresult").each(function(minionresult){
                                
                                if(min.minion == minionresult.get("slug") )
                                {        
                                    min.data["type"] = "page"; //we inject the type
                                    minionresult.set("data",min.data);
                                }
                             });
                        });
                        
                        this.$el.addClass("show");
                        console.log("RENDER RESULT!")

                        
                    }
                    else{
                        var page = this.model.get("pages").get(data.page._id);
                        page.set("minions",data.page.minions);
                        
                        _.each(data.page.minions,function(min){                            
                             page.get("minionsresult").each(function(minionresult){                                 
                                if(min.minion == minionresult.get("slug"))
                                {
                                    
                                    min.data["type"] = "page"; //we inject the type
                                    minionresult.set("data",min.data);
                                }
                             });
                        });
                        
                    }
                    
                    if(!this.isFirstPageUpdate)
                    {   
                        this.isFirstPageUpdate = true;
                        
                    }
                    
                    
                    
                    
                break;
                
            }
        },
        log:function(id,msg,completed){
            if(completed == false)    
            {
                this.$el.find(".log ul").prepend('<li class="status-'+id+'"><span class="text">'+msg+'</span><span class="loading"></span></li>');
            }
            else{
                this.$el.find(".log ul .status-"+id+" .text").text(this.$el.find(".log ul .status-"+id+" .text").text()+"...Done!" );
                this.$el.find(".log ul .status-"+id).addClass("completed");
            }
        },
        sendTestRequest: function(ev){
            ev.preventDefault();
            
            var url = this.$el.find("input[type=text]").val();
            
            if( url == "" ) return this.showError("Url cannot be empty");            
            else if( !App.helpers.ValidUrl(url) ) return this.showError("The url you entered is not valid");
            else{
                this.$el.find("input").attr("disabled","disabled");
                this.$el.find(".btn").val("Validating");                
                
                var w = window.innerWidth?window.innerWidth: window.width;
                var h = window.innerHeight?window.innerHeight: window.height;                
                
                App.config.needToConfirm = true; 
                
                App.io.emit('test',{url:url,w: w, h:h});                                             
            }
            
        },
        createResultsPage:function(){
   
        },
        showError: function(msg){
            alert(mgs);
        },
        render:function(){
        
        },
        renderResult: function(){
                  console.log("test:renderResult");
                  this.$el.find(".result").show();
                 
                  this.model.get("pages").at(0).get("minionsresult").each(function(minion){                      
                      var minionBox = new App.Views.MinionBox({ model: minion });                      
                      this.$el.find(".result-page .row").append(minionBox.render().el);
                      minionBox.resize();                                            
                  }, this);
            
                   /* this.model.get("blueprint").get("siteMinions").each(function(minion){                      
                      var minionBox = new App.Views.MinionBox({ model: minion });                      
                      this.$el.find(".result-site .row").append(minionBox.render().el);
                      minionBox.resize();                                            
                  }, this);*/
            
                    this.$el.find('.result  .row').isotope({
                        itemSelector: '.minionbox',
                        layoutMode: 'masonry',
                        masonry: {
                            columnWidth: this.$el.find('.result  .row').width()/6,
                            gutter:0
                        }
                    });
                  return this;
        }
});

App.Views.MinionBox  = Backbone.View.extend({                
                initialize: function(){               
                    console.log("minionbox:initialize:",this.model.get("slug"));
                    this.model.on("change",this.change,this);
                    //this.initMinion();       
                    console.log(this.model.get("slug")," init:data ",this.model.get("data"));
                    if( this.model.get("data").type)                    
                    {
                        console.log("Init FROM INIT");
                        this.initMinion();
                    }
                },            
                events: {
                    "change select": "changesize"                    
                },             
                change:function(){
                    console.log("change:",this.model.get("slug"),this.model.get("data"));
                    if(typeof this.model.get("data") !== "undefined")
                    {
                        console.log("Init FROM CHANGE");
                        this.initMinion();
                    }
                },
                initMinion:function(){ 
                    
                    if(this.initialized)
                        return false;
                    this.initialized = true;
                    console.log(this.model.get("slug"),": initMinion",this.$el.find(".minionbox")[0]);
                    
                    this.$el.find(".minionbox").removeClass("page-type site-type").addClass("page-type");                    
                    
                    this.$el.find(".minionbox > .panel > .progress > .progress-bar").stop().animate({ width: "100%"},500,function() {  $(this).parents(".minionbox").removeClass("loading")   });
                    
                    this.model.get("actions").init(this);                                                            
                    this.$el.find(".minionbox .panel .panel-content").show();
                    this.model.get("actions").render(this);                
                    
                },               
                render: function(){  
                    console.log("render:",this.model.get("slug"));
                    if($("#minion-box-template").length == 0) return console.warn("Missing minion html template");                    
                    this.$el.html( _.template( $("#minion-box-template").html(),  this.model.toJSON() )  );                    
                    this.$el.find(".minionbox .panel .panel-content").html(JST["minions/"+this.model.get("slug")]);
                    this.$el.find(".minionbox > .panel > .progress > .progress-bar").stop().animate({ width: "90%"},10000,function() {    });
                    return this;                    
                },               
                changesize:function(){
                  this.model.set("size",this.$el.find(".panel-heading select[name=size]").val());                                    
                  this.resize();                    
                 // this.model.get("actions").resize(this);                    
                },                              
                resize: function(){                    
                    var size = this.model.get("size").split("x");                
                    var colsize =  "col-xs-" + (parseInt(size[0])*2);
                    var rowsize = parseInt(size[1]);
                    
                    var cols = (parseInt(size[0]));
                    var rows = (parseInt(size[1]));
                    var $this = this;
                    this.$el.find(".minionbox").removeClass(function(){
                        var out = [];
                        for(var i = 1; i<=12; i++){
                            out.push("col-xs-"+i);
                            out.push("mn-size-le-"+i+"-cols");
                            out.push("mn-size-eq-"+i+"-cols");                            
                            out.push("mn-size-eq-"+i+"-rows");
                        }
                        return out.join(" ");
                    }).addClass(colsize).addClass(function(){ 
                        var out = [];
                        for(var i = 1; i<=4; i++){
                            if(i<cols){
                                out.push("mn-size-le-"+i+"-cols");
                            }
                            if(i==cols){
                                out.push("mn-size-eq-"+i+"-cols");
                            }
                            if(i==rows){
                                out.push("mn-size-eq-"+i+"-rows");
                            }                            
                        }                        
                        return out.join(" ");
                    });
                    
                    var onewidth = this.$el.find(".minionbox").outerWidth()/ (parseInt(size[0])*2);                    
                    this.$el.find(".minionbox .panel").css("height",onewidth * rowsize * 2 );                                        
                }
            });




App.Router = {

            };

App.data = { testid: null };
App.models = { test:null };
App.collections = {};
App.views = { }

App.generateUIForTest = function() { return alert("Not coded yet"); }
App.initialize = function(args) {
                    if(args.testid){
                        App.data.testid = args.testid ;
                        App.generateUIForTest();
                    }
                    else{
                        App.prepareUIforNewTest();
                    }
                 }



App.prepareUIforNewTest =  function(args) {
   App.io = io.connect();
    
   if(BootstrapDataBlueprint){
       
        var homeMinionsCollection =  new App.Collections.Minions();
        var siteMinionsCollection =  new App.Collections.Minions();
        var pageMinionsCollection =  new App.Collections.Minions();
        var homeMinions = BootstrapDataBlueprint.homeMinions;               
        var siteMinions = BootstrapDataBlueprint.siteMinions;  
        var pageMinions = BootstrapDataBlueprint.pageMinions;      
       
       
        _.each(BootstrapDataMinions,function(dataminion,index){ 
            dataminion.actions = dataminion.gen();            
            dataminion.gen = null;
            _.each(homeMinions,function(item,index){           
                
                if(dataminion.slug == item.slug)
                {
                
                    var min = new App.Models.Minion(dataminion);
                    min.set("size",item.size);
                    homeMinionsCollection.add(min);
                }
            });
            _.each(siteMinions,function(item,index){           
                if(dataminion.slug == item.slug)
                {
                    var min = new App.Models.Minion(dataminion);
                    min.set("size",item.size);
                    
                    siteMinionsCollection.add(min);
                }
            })            
            _.each(pageMinions,function(item,index){           
                if(dataminion.slug == item.slug)
                {
                    var min = new App.Models.Minion(dataminion);
                    min.set("size",item.size);
                    pageMinionsCollection.add(min);
                }
            })            
        }); 
       
        App.models.blueprint = new App.Models.BluePrint({
                name: BootstrapDataBlueprint.name,
                slug: BootstrapDataBlueprint.slug,
                siteMinions:siteMinionsCollection,
                pageMinions:pageMinionsCollection,
                homeMinions: homeMinionsCollection                
            });       
        App.models.test =  new App.Models.Test({blueprint: App.models.blueprint, newFromUI: true });
        App.views.test = new App.Views.Test({el:$("#main-check"),model: App.models.test });

   }
    
    
    


}





