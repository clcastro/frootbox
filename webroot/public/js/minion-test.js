
   var htmleditor = CodeMirror.fromTextArea(document.getElementById("resultHtml"), {       
    lineNumbers: true,
    mode: "htmlembedded",
    theme:"monokai"
   });
     var csseditor = CodeMirror.fromTextArea(document.getElementById("resultCSS"), {       
    lineNumbers: true,
    mode: "css",
    theme:"monokai"
   });
    var jseditor = CodeMirror.fromTextArea(document.getElementById("resultJS"), {       
    lineNumbers: true,
    mode: "javascript",
    theme:"monokai"
   });
    
    var responseeditor = CodeMirror.fromTextArea(document.getElementById("pageresponse"), {       
    lineNumbers: true,
    mode: "javascript",
    theme:"monokai"
   });
    var siteresponseeditor = CodeMirror.fromTextArea(document.getElementById("siteresponse"), {       
    lineNumbers: true,
    mode: "javascript",
    theme:"monokai"
   });     
        
    
    $(function() {            
             /*************** BACKBONE ****************************/
            //Models
            window.MinionBoxModel = Backbone.Model.extend({
                 defaults: {
                    size:  "1x1",
                    name: "Result",
                    slug: "result",
                    url: "http://localhost",
                    isPageMinion: true,
                    isSiteMinion: true,
                    isDummy: true,
                    defaultConfig: {},
                    resultHTML: "",
                    resultCSS: "",
                    resultJS: "",
                    resultJSObj: null,
                    availableSizes: ["1x1","2x1","3x1","4x1","2x2","3x2","4x2"],
                    data:{},
                    ids:{pageid:"abcd",siteid:"efgh"}
                  },                  
                  setData:function(data){                    
                    this.set("data",data );
                  },
                  setJS:function(fun){                    
                    this.set("resultJSObj",fun() );                    
                  },
                  changeBoxSize: function() {            
                  },
                  getMinID:function(){
                    return "minion-"+this.get("slug")+"-"+this.get("ids").pageid+"-"+this.get("ids").siteid;                    
                  },
                  clean:function(){
                      
                      this.set("data",null );
                      this.set("resultJSObj",null );                    
                      this.set("resultJS","" );
                      this.set("resultHTML","" );                    
                      this.set("resultCSS","" );                    
                  }
            });
        
            
            window.MinionBoxCollection = Backbone.Collection.extend({
              model: MinionBoxModel
            });        
        
            window.ResultView = Backbone.View.extend({
              tagName: 'div',
              editMinionView: null,
              initialize: function(){               
                  this.render();
              },
              getEditMinionView: function(){
                  return this.editMinionView;
              },
              render: function(){
                  var index = 0;
                  this.collection.each(function(minion){                      
                      var minionBox = new MinionBox({ model: minion });                      
                      this.$el.append(minionBox.render().el);
                      minionBox.resize();
                      if(index == 0){
                        this.editMinionView = minionBox;
                      }
                      index++;
                  }, this);
                  return this;
              }
            });

            window.MinionBox = Backbone.View.extend({            
                tagName: 'div',
                initialize: function(){               
                    this.render();
                    this.model.on("change:size",$.proxy(this.resize,this));
                    //this.model.on("change:resultHTML",$.proxy(this.updateResultHTML,this));
                    //this.model.on("change:resultCSS",$.proxy(this.updateResultCSS,this));
                    this.model.on("change:resultJSObj",$.proxy(this.initMinion,this));                    
                },            
                events: {
                    "change select": "changesize"                    
                },             
                initMinion:function(){
                    if(!this.model.get("resultJSObj")) return;
                    
                    this.$el.find(".minionbox").removeClass("page-type site-type");
                    this.$el.find(".minionbox").addClass( this.model.get("data").type+"-type" );
                    this.model.get("resultJSObj").init(this);                    
                    
                    this.$el.find(".minionbox .panel .panel-content").show();                    
                    this.model.get("resultJSObj").render(this);
                    this.$el.find(".minionbox > .panel > .progress > .progress-bar").animate({ width: "100%"},500,function() {  $(this).parents(".minionbox").removeClass("loading")   });
                    
                },
                render: function(){                                                                                        
                    this.$el.html( _.template( $("#box_template").html(),  this.model.toJSON() )  );                                            
                    return this;                    
                },
                changesize:function(){
                  this.model.set("size",this.$el.find(".panel-heading select[name=size]").val());
                  $('.testarea .row').isotope();
                  if(!this.model.get("resultJSObj")) return;
                  this.model.get("resultJSObj").resize(this);                    
                },
                updateResultHTML: function(){                                        
                    
                    this.$el.find(".minionbox .panel .panel-content").html("");
                    this.$el.find(".minionbox .panel .panel-content").html(this.model.get("resultHTML"));
                    
                },
                updateResultCSS: function(){
                    
                    var cssid = "#css-"+this.model.getMinID();                    
                    $(cssid).remove();
                    $("body").append( '<style type="text/css" id="'+cssid+'">'+this.model.get("resultCSS")+'</style>'  )

                },
                prepare:function(){
                    this.$el.find(".minionbox .panel .panel-content").html("").css("display","none");
                },
                resize: function(){                    
                    var size = this.model.get("size").split("x");                
                    var colsize =  "col-xs-" + (parseInt(size[0])*2);
                    var rowsize = parseInt(size[1]);
                    
                    var cols = (parseInt(size[0]));
                    var rows = (parseInt(size[1]));
                    var $this = this;
                    this.$el.find(".minionbox").removeClass(function(){
                        var out = [];
                        for(i = 1; i<=12; i++){
                            out.push("col-xs-"+i);
                            out.push("mn-size-le-"+i+"-cols");
                            out.push("mn-size-eq-"+i+"-cols");                            
                            out.push("mn-size-eq-"+i+"-rows");
                        }
                        return out.join(" ");
                    }).addClass(colsize).addClass(function(){ 
                        var out = [];
                        for(i = 1; i<=4; i++){
                            if(i<cols){
                                out.push("mn-size-le-"+i+"-cols");
                            }
                            if(i==cols){
                                out.push("mn-size-eq-"+i+"-cols");
                            }
                            if(i==rows){
                                out.push("mn-size-eq-"+i+"-rows");
                            }                            
                        }
                        if($this.model.get("isDummy")) out.push("mn-dummy");
                        return out.join(" ");
                    });
                    
                    var onewidth = this.$el.find(".minionbox").outerWidth()/ (parseInt(size[0])*2);                    
                    this.$el.find(".minionbox .panel").css("height",onewidth * rowsize * 2 );                                        
                }
            });
        
    
            window.editMinionCollection = new MinionBoxCollection();
        
            window.editMinionModel = new MinionBoxModel({isDummy:false, name: $("#name").val(),slug:$("#slug").val()   });                                    
            window.editMinionCollection.add(editMinionModel);            
            var sizes = ["1x1","2x1","1x1"];            
            for(i=0;i<sizes.length; i++){
                editMinionCollection.add(  new MinionBoxModel({ name:"Dummy "+i, size:sizes[i] })   );
            }
                    
        
            window.resultView = new ResultView({ el: $(".testarea .row"), collection: editMinionCollection });
        
            function updateMinionView(){
                
                
                editMinionModel.clean();
                editMinionModel.set( "resultHTML" , htmleditor.getValue() );
                editMinionModel.set( "resultCSS" , csseditor.getValue() );                                                               
                resultView.getEditMinionView().updateResultHTML();
                resultView.getEditMinionView().updateResultCSS();
                
                var lejs = "function(){"+jseditor.getValue()+"}";
                var type = $("[name=type]:checked").val();
                switch(type){
                    case "page": 
                        setTimeout('window.editMinionModel.setData('+ responseeditor.getValue()+' );',0);        
                    break;
                    case "site": 
                        setTimeout('window.editMinionModel.setData('+ siteresponseeditor.getValue()+' );',0);
                    break;
                }
                
                setTimeout("window.editMinionModel.setJS("+lejs+")",1);
                
            }
        
    
            $("#test").on("click",function(ev){
                ev.preventDefault();
                resultView.getEditMinionView().prepare();
                updateMinionView();
            });
        
            $("#update").on("click",function(ev){
                ev.preventDefault();
                $this = $(this);
                $this.addClass("disabled").attr("disabled",true);
                $(".actionpanel > .progress > .progress-bar").css("width","60%");
                $.post(
                        "/dev/leminions/"+$("#minionid").val()+"/edit",
                       {
                            resultHtml : htmleditor.getValue(),
                            resultCSS :    csseditor.getValue(),
                            resultJS :    jseditor.getValue(),
                            examplePageResponse :    responseeditor.getValue(),
                            exampleSiteResponse :    siteresponseeditor.getValue(),
                            size : editMinionModel.get("size")
                       },
                       function(res){
                         $this.removeClass("disabled").removeAttr("disabled");
                           $(".actionpanel > .progress > .progress-bar").css("width","100%");
                       },
                       'json'                        
                );
                
            })
        
            $('.testarea .row').isotope({
                itemSelector: '.minionbox',
                layoutMode: 'masonry',
                masonry: {
                    columnWidth: $(".testarea .row").width()/6,
                    gutter:0
                }
            });
        
            setTimeout(function(){ $('.testarea .row').isotope(); },50)
            setTimeout(function(){ updateMinionView(); } ,400);
            
    });