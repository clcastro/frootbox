var passport = require('passport'),
    http = require('follow-redirects').http;

var https = require('follow-redirects').https;
    $ = require('jquery'),
    url = require('url'),
    validator = require('validator'),
    FROOTCONFIG = require('config');

//Models
var User = require('./models/user'),
    Plan = require('./models/plan'),
    Account = require('./models/account'),
    AccountPlan = require('./models/accountplan'),
    UserAccount = require('./models/useraccount'),
    Minion = require('./models/minion'),
    BluePrint = require('./models/blueprint'),
    TestRequest = require('./models/testrequest'),
    Page = require('./models/page'),
    Test = require('./models/test');

//Controllers
var MinionsController = require('./controllers/minions');
var ContactController = require('./controllers/contact');
var IncludeController = require('./controllers/include');

var pre = FROOTCONFIG.general.prefix;

module.exports = function (app) {
    app.get('/', function (req, res) {
       res.end("Soon");

    });
    app.get(pre, function (req, res) {
        console.log("HOME");
       res.render('index', { bodyclass:'page-home',user:req.user });
    });
    app.get(pre + '/contact', ContactController.viewContactForm);
    app.post(pre + '/contact', ContactController.sendContactRequest);

    /** Auth & Sign up **/

    app.get(pre + '/auth/google', passport.authenticate('google'))
    app.get('/auth/google/return',
    passport.authenticate('google', { successRedirect: pre + '/dashboard',
    failureRedirect: pre + '/login' }));

    app.get(pre + '/auth/facebook', passport.authenticate('facebook', { scope: 'email' }) );
    app.get('/auth/facebook/callback',
    passport.authenticate('facebook', { successRedirect: pre + '/dashboard',
    failureRedirect: pre + '/login' }));

    app.get(pre + '/auth/github',
    passport.authenticate('github',{scope:'user,user:email,repo,read:repo_hook,write:repo_hook,'}));

    app.get('/auth/github/callback',
    passport.authenticate('github', { failureRedirect: pre + '/login' }),
      function(req, res) {
        // Successful authentication, redirect home.
        res.redirect(pre + '/dashboard');
    });


    app.get(pre + '/leminions',MinionsController.viewAllMinions);
    app.get(pre + '/leminions/new',MinionsController.addMinion);
    app.post(pre + '/leminions/new',MinionsController.saveNewMinion);
    app.get(pre + '/leminions/:id/edit',MinionsController.editMinion);
    app.post(pre + '/leminions/:id/edit',MinionsController.updateMinion);

    app.get(pre + '/leminions/:id/test',MinionsController.testMinion);
    app.post(pre + '/leminions/:id/test',MinionsController.updateTestMinion);


    app.get(pre + '/minions/scripts/all.js',IncludeController.JsAllMinions);
    app.get(pre + '/minions/css/all.css',IncludeController.CssAllMinions);

    app.get(pre + '/signup', function(req, res) {
        res.render('signup', { bodyclass:'page-signup-alt',user:req.user });
    });

    app.post(pre + '/signup', function(req, res) {
        
        if( !validator.isEmail(req.body.signup_email)){
        
            return res.render('signup', { err:{body:"The email is not valid"} });
        }
        else if( req.body.signup_email != req.body.signup_email2 ){
        
            return res.render('signup', { err:{body:"The emails must be the identical"} });
        }
        else if( validator.trim(req.body.signup_password) == ""){
        
            return res.render('signup', { err:{body:"The password cannot be empty"} });
        }
        else if( req.body.signup_password != req.body.signup_password2 ){
        
            return res.render('signup', { err:{body:"The passwords must be the identical"} });
        }
        else{
            User.register(new User({ email : req.body.signup_email }), req.body.signup_password, function(err, user) {
                if (err) {
                    
                    console.log(err);
                    return res.render('signup', { err:err });
                }
                else{
                    user.asignBasicPlanWithNewUser(function(err,acc){
                        res.redirect(pre + '/dashboard');
                    });
                }

            });
        }
    });
    app.get(pre + '/login', function(req, res) {
        res.render('login', { bodyclass: 'page-signin-alt',user:req.user });
    });

    app.post(pre + '/login', passport.authenticate('local'), function(req, res) {
    res.redirect(pre + '/dashboard');
    });

    app.get(pre + '/logout', function(req, res) {
        req.logout();
    res.redirect(pre);
    });

    /** Account related paths **/
    app.get(pre + '/dashboard', function(req, res) {
        if(req.user && req.user._id){
            res.render('dashboard', { user : req.user });
        }
        else{
            res.redirect('/');
        }
    });


    app.get(pre + '/account/user/profile', function(req, res) {
        if(req.user && req.user._id){
            res.render('profile', { user : req.user });
        }
        else{
            res.redirect('/');
        }
    });
    app.get(pre + '/account/user/profile/edit', function(req, res) {
        if(req.user && req.user._id){
            res.render('profile-update', { user : req.user });
        }
        else{
            res.redirect('/');
        }
    });

    /**Test related endpoints ***/
    app.post(pre + '/testrequest',function(req, res){
        if(req.user && req.user._id){
            res.json({ status: "error",error:"Not implemented yet" });
        }
        else{

             User.findOne({email: "guest@frootbox.com" }, function(err,user){
                  Plan.findOne({slug: "guest" }, function(err,guestplan){
                        console.log("Creating test request");
                        var testrq = new TestRequest({
                            userid : user,
                            bluePrintId : guestplan.blueprintid,
                            isFromGuestUser: true,
                            created: new Date(),
                            clientIp: getUserIp(req),
                            clientAgent: req.headers['user-agent'],
                            url: req.body.url,
                            width: req.body.w,
                            height: req.body.h
                        });
                      testrq.save(function(err,testrequest){
                        if(err){
                            res.json({ status: "error",error:err });
                        }else{
                            console.log("test request created");
                            TestRequest.onUpdate(testrequest._id,['completed','failed','active'],function(err,request){                                  
                                if(err)  return console.log(err);
                                console.log('testrequest, updated:',request.status); 
                                if(request.status == 'completed')
                                {
                                    console.log("searching for %s",testrequest.testid);
                                    Test.findById(testrequest.testid,function(err,test){
                                        if(err) return console.log("error loading the test");
                                        console.log('activating onUpdate');
                                        Test.onUpdate(test._id,['created','completed','failed','active'],function(err,test){                                  
                                            console.log('test, updated:',test.status);
                                            console.log(test.log);
                                        });
                                    });
                                }
                            });                                                        
                            res.json({ status: "success",requestid:testrequest._id });
                            
                        }
                      });
                  });


             });
        }
    });
    
    // Setup the ready route, and emit talk event.
    app.io.route('ready', function(req) {
        
        
        req.io.emit('talk', {
            message: 'io event from an io route on the server'
        })
        
        
    });    
    
    //HERE NOT POST PLEEEEEASE!
    
    app.io.route('test', function(req) {
            console.log(req.data);
            User.findOne({email: "guest@frootbox.com" }, function(err,user){
                  Plan.findOne({slug: "guest" }, function(err,guestplan){
                        var testrq = new TestRequest({
                            userid : user,
                            bluePrintId : guestplan.blueprintid,
                            isFromGuestUser: true,
                            created: new Date(),
                           /* clientIp: getUserIp(req),*/
                            /*clientAgent: req.headers['user-agent'],*/
                            url: req.data.url,
                            width: req.data.w,
                            height: req.data.h
                        });
                      testrq.save(function(err,testrequest){
                        if(err){                            
                            req.io.emit('testupdate',{ status: "error",error:err });
                        }else{
                                                        
                            TestRequest.onUpdate(testrequest._id,['completed','failed','active'],function(err,request){                                  
                                if(err)  return console.log(err);
                                console.log('testrequest updated:',request.status);
                                                              
                                switch(request.status){
                                    case 'active':
                                        req.io.emit('testupdate', {status:'active', requestid:request._id });
                                    break;
                                    case 'completed':
                                        if(request.wasTestCreated){
                                            req.io.emit('testupdate', {status:'test-created',testid:request.testid, requestid:request._id });                                                                                                                                    
                                            Test.findById(request.testid,function(err,test){
                                                test.html = "";
                                                console.log("Test data found LOG:",test.log);
                                                req.io.emit('testupdate', {status:'test-data',testid:request.testid, data: test });
                                                
                                                Test.onUpdate(request.testid,['created','completed','failed','active'],function(err,test){                                  
                                                    
                                                    console.log('test, updated:',test.status,test.log.length);
                                                    
                                                    if(test.pages.length > 0){
                                                        if(test.pages[0].isHome)
                                                        {
                                                            Page.onUpdate(test.pages[0].pageid,['created','completed','failed','active'],function(err,page){                                  
                                                                console.log('page, updated:',page.minions.length);
                                                                page.html = "";
                                                                req.io.emit('testupdate', {status:'page-update',testid:request.testid, page: page });            
                                                            });
                                                        }
                                                    }
                                                    req.io.emit('testupdate', {status:'test-update',testid:request.testid, data: test.log });
                                                });                       
                                                                         
                                                
                                            });
                                            
                                                
                                            
                                        
                                            /*Test.onUpdate(request.testid,['created','completed','failed','active'],function(err,test){
                                                req.io.emit('testupdate', {status:'test-updated',testid:request.testid, data: test });
                                            });*/
                                        }
                                        else{
                                            req.io.emit('testupdate', {status:'failded-creating',status:request.httpStatus, requestid:request._id });
                                        }
                                    break;
                                    
                                }
                                
                            });             
                            
                            
                            //res.json({ status: "success",requestid:testrequest._id });
                            req.io.emit('testupdate', {status:'success', requestid:testrequest._id });
                            
                        }
                      });
                  });


             });
        
        /*req.io.emit('talk', {
            message: 'io event from an io route on the server'
        })*/
        
        
    });    






    /*** This route adds some basic information that has to be present in the System ***/
    app.get(pre + '/install/loremipsumnomewei', function (req, res) {
        Plan.findOne({slug: "guest" }, function(err,guestplan){
            if(!guestplan)
            {
                var guestplan = Plan({
                            created:new Date(),
                            lastModified:new Date(),
                            type: "free",
                            monthlyPrice: 0,
                            yearlyPrice: 0,
                            name: "Guest",
                            slug: "guest",
                            maxPagesPerTest:5,
                            maxTestsPerDay:-1,
                            maxTestsPerDayPerDomain:3,
                            minutesBetweenTests:30,
                            canProgrammedTest: false,
                            canHookTest: false,
                            maxProgrammedTests: 0,
                            maxHookTests: 0
                        });
                guestplan.save(function(err,pln){
                    if(err) { return console.log("Error creating pln");  }
                    res.write("Guest Plan Created <br />");

                    /**We create the guest user**/
                    User.findOne({email: "guest@frootbox.com" }, function(err,user){
                        if(!user)
                        {
                            var user = User({
                                                email:"guest@frootbox.com",
                                                name: {
                                                        familyName: "Guest",
                                                        givenName: "User"
                                                      },
                                                displayName : "Guest User",
                                                openid: "",
                                                fbid: "",
                                                githubId:"",
                                                twitterUsername: "",
                                                githubUsername: "",
                                                photos : [{value:""}],
                                                birthdate: new Date(),
                                                created:new Date(),
                                                lastLogin:new Date(),
                                                emailValidated: true,
                                                fbMeta:null,
                                                githubMeta:null,
                                                isGuestUser: true


                                            });
                            user.save(function(err,theuser){
                                if(err) { return console.log("Error creating theuser");  }
                                res.write("Default User Created<br />");
                                    /**** START ACCOUNT ****/
                                    Account.findOne({email: "guest@frootbox.com" }, function(err,account){
                                        if(!account)
                                        {
                                            var account = Account({
                                                                created: new Date(),
                                                                users: [theuser],
                                                                plans:[guestplan]
                                                            });
                                            account.save(function(err,theacc){
                                                if(err) { return console.log("Error creating theacc");  }
                                                res.write("Default Account  Created<br />");

                                                var uaccount = UserAccount({
                                                                created: new Date(),
                                                                userid: theuser,
                                                                accountid: theacc,
                                                                acl:"admin"
                                                            });
                                                uaccount.save(function(err,theuacc){
                                                    if(err) { return console.log("Error creating theuacc");  }
                                                    res.write("Default UserAccount  Created<br />");
                                                    var accplan = AccountPlan({
                                                                created: new Date(),
                                                                accountid: theacc,
                                                                planid: guestplan,
                                                                hooks: [],
                                                                programmedTasks:[]
                                                            });
                                                    accplan.save(function(err,theaccplan){
                                                        if(err) { return console.log("Error creating accplan");  }

                                                        res.write("Default AccountPlan  Created<br />");

/*********************************************************************************/
                                                        Minion.findOne({slug: "meta" }, function(err,minion){
                                                            if(!minion)
                                                            {
                                                                var minion = Minion({
                                                                            isPageMinion:true,
                                                                            isSiteMinion:false,
                                                                            name: "Meta",
                                                                            slug: "meta",
                                                                            created: new Date(),
                                                                            lastModified: new Date(),
                                                                            url:"http://www.frootbox.com:6969/minions/page/meta",
                                                                            defaultConfig:[],
                                                                            resultRequiredScripts:[],
                                                                            resultHtml:"",
                                                                            resultJS: "",
                                                                            resultDefaultSize:"2x2"
                                                                        });
                                                                minion.save(function(err,min){
                                                                    res.write("minion  meta created<br />");
                                                                    var minion2 = Minion({
                                                                            isPageMinion:false,
                                                                            isSiteMinion:true,
                                                                            name: "Must",
                                                                            slug: "must",
                                                                            created: new Date(),
                                                                            lastModified: new Date(),
                                                                            url:"http://www.frootbox.com:6969/minions/site/must",
                                                                            defaultConfig:[],
                                                                            resultRequiredScripts:[],
                                                                            resultHtml:"",
                                                                            resultJS: "",
                                                                            resultDefaultSize:"2x2"
                                                                        });
                                                                     minion2.save(function(err,min2){
                                                                        res.write("minion  must created<br />");

                                                                         var blueprint = BluePrint({
                                                                                created:new Date(),
                                                                                modified: new  Date(),
                                                                                name: "Default Guest",
                                                                                slug: "default-guest",
                                                                                minions:[ min,min2 ],
                                                                                minionsConfig:{ "meta":{},"must":{} },
                                                                                maxPages: 10,
                                                                                timeoutSeconds: 300
                                                                            });
                                                                           blueprint.save(function(err,blue){
                                                                                res.write("Blueprint created<br />");

                                                                               pln.blueprintid = blue;
                                                                               pln.save(function(err,pln){
                                                                                   res.write("Plan updated<br />");
                                                                                   res.end();
                                                                               });




                                                                           });
                                                                     });
                                                                });
                                                            }
                                                        });


/************************************************************************/





                                                    });




                                                });


                                            });
                                        }
                                    });

                                /******END ACCOUNT ******/
                            });
                        }
                    });





                });
            }
        });
        Plan.findOne({slug: "basic" }, function(err,plan){
            if(!plan)
            {
                var plan = Plan({
                            created:new Date(),
                            lastModified:new Date(),
                            type: "free",
                            monthlyPrice: 0,
                            yearlyPrice: 0,
                            name: "Basic",
                            slug: "basic",
                            maxPagesPerTest:10,
                            maxTestsPerDay:10,
                            maxTestsPerDayPerDomain:6,
                            minutesBetweenTests:10,
                            canProgrammedTest: true,
                            canHookTest: true,
                            maxProgrammedTests: 1,
                            maxHookTests: 1
                        });
                plan.save(function(err,pln){
                    res.write("Basic Plan Created<br />");
                });
            }
        });



    });

};


function getUserIp(req){
    return req.headers['x-forwarded-for'] ||
     req.connection.remoteAddress ||
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress;
}
