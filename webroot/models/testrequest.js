var mongoose = require('mongoose'),
    AccountPlan = require('./accountplan'),
    User = require('./user'),
    Test = require('./test'),
    UserBluePrint = require('./userblueprint'),
    BluePrint = require('./blueprint'),
    Taskify = require('./taskify'),
    Schema = mongoose.Schema;

var TestRequestSchema = new Schema({    
    accountplanid : { type: Schema.ObjectId, ref: 'AccountPlan' },
    userid : { type: Schema.ObjectId, ref: 'User' },
    testid : { type: Schema.ObjectId, ref: 'Test' },
    userBluePrintId : { type: Schema.ObjectId, ref: 'UserBluePrint' },
    bluePrintId : { type: Schema.ObjectId, ref: 'BluePrint' },
    wasTestCreated: { type: Boolean, default:false},
    isFromGuestUser: { type: Boolean, default:true},
    created: Date,    
    httpStatus: Number,
    clientIp: String,
    clientAgent: String,
    url: String,
    width: Number,
    height: Number
});

TestRequestSchema.plugin(Taskify);



module.exports = mongoose.model('TestRequest', TestRequestSchema);