var mongoose = require('mongoose'),
    User = require('./user'),
    Account = require('./account'),
    Schema = mongoose.Schema;

var UserAccountSchema = new Schema({    
    userid : { type: Schema.ObjectId, ref: 'User' },
    accountid : { type: Schema.ObjectId, ref: 'Account' },
    created: Date,
    acl: String 
});

module.exports = mongoose.model('UserAccount', UserAccountSchema);