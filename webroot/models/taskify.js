var util = require('util');
var mongoose = require('mongoose');

module.exports = function(schema, options) {
    options = options || {};
    options.lastModifiedField = options.lastModifiedField || 'lastModified';
    options.statusField = options.statusField || 'status';
    options.retryField = options.retryField || 'retry';
    options.retryTimesField = options.retryTimesField || 'retryTimes';
    options.totalRetryTimesField = options.retryTimesField || 'totalRetryTimes';
    options.failedTimesField = options.retryTimesField || 'failedTimes';
    options.progressFramesField  = options.progressFramesField || 'progress';
    options.progressTotalFramesField  = options.progressTotalFramesField || 'progressTotal';
    options.logField  = options.logField || 'log';
    
    var schemaFields = {};
  
    schemaFields[options.lastModifiedField] = Date;
    schemaFields[options.statusField] = { type:String , default:"created" };
    schemaFields[options.retryField] = { type:Boolean , default:false };    
    schemaFields[options.retryTimesField] = { type:Number , default:0 };
    schemaFields[options.totalRetryTimesField] = {type:Number , default:0 };
    schemaFields[options.failedTimesField] = {type:Number , default:0 };
    schemaFields[options.progressFramesField] = Number;
    schemaFields[options.progressTotalFramesField] = Number;
    schemaFields[options.logField] = Array;
        
    schema.add(schemaFields);
        
    schema.statics.watchingTasksIds = [];
    
    schema.statics.addWatchingTask = function (id){
        var self = this;  
        if( !self.constructor.watchingTasksIds)
        {
            self.constructor.watchingTasksIds = [id];
        }
        else if( self.constructor.watchingTasksIds.indexOf(id) <0){
            self.constructor.watchingTasksIds.push(id);
        }
    }
     schema.statics.removeWatchingTask = function (id){
        var self = this;         
        if(self.constructor.watchingTasksIds){
            var index = self.constructor.watchingTasksIds.indexOf(id);
            if(index >= 0)
            {
                  self.constructor.watchingTasksIds.splice(index, 1);
            }           
        }
    }
    schema.methods.setStatusActive = function(cb){
        var self = this;         
        self.status = 'active';
        self.save(function(err,tsk){   
             //here we remove the task from current jobs
            self.constructor.addWatchingTask(tsk._id);
            cb(err,tsk );
        });          
    }
    
    schema.methods.setStatusCompleted = function(cb){
        var self = this;         
        self.status = 'completed';
        self.save(function(err,tsk){    
            //here we remove the task from current jobs
            self.constructor.removeWatchingTask(tsk._id);
            cb(err,tsk );
        });          
    }
    schema.methods.setStatusFailed = function(cb){
        var self = this;    
        if(self.get(options.retryField))
        {
            self.set(options.failedTimesField, self.get(options.retryTimesField) + 1 );
            if( self.get(options.retryTimesField) < self.get(options.totalRetryTimesField) ){                                    
                self.set(options.retryTimesField, self.get(options.retryTimesField) + 1 )
                self.status = 'active';       
            }
            else{
                self.status = 'failed';       
            }
        }
        else{
            self.status = 'failed';       
        }
        self.constructor.removeWatchingTask(self._id);
        self.save(function(err,tsk){
            //here we remove the task from current jobs
            cb(err, tsk );
        }); 
    }
    
    schema.pre('save', function(next) {                
        this[options.lastModifiedField] = new Date();        
        next();
    });
    
     
      
    schema.statics.onUpdate = function (id,statuses,cb) {         
        var taskify = { findTimeout : 0 };
        var self = this;
        self.findById(id,function(err,task){
            if(err) return cb(err,null);
            if(!task) return cb(null,null);
            taskify['lastLastModified'] = task.get(options.lastModifiedField);
            if(!taskify['lastLastModified']) taskify['lastLastModified'] = new Date();
            findMyTask();
        });
        
        function findMyTask()
        {
            var query = self.findOne({_id: id });
                query.where(options.lastModifiedField).gt(taskify['lastLastModified']);
            
                if(statuses && statuses.length >0 ){
                    query.where(options.statusField).in(statuses);                
                }             
                query.exec(function(err,task){                                        
                    if(!task)                    
                        taskify['findTimeout'] = setTimeout(findMyTask,1000);                    
                    else
                    {
                        clearTimeout(taskify['findTimeout']);
                        taskify['findTimeout'] = 0;
                        taskify['lastLastModified'] = task.get(options.lastModifiedField);
                        
                        var ret = cb(null,task);                        
                        if(ret !== true ) taskify['findTimeout'] = setTimeout(findMyTask,1000);
                        else{
                            console.log("we stop here");
                        }
                    }
                });
        }
    }
    
  
    
    
    schema.statics.onCreate = function(cb){
        var taskify = { findTimeout : 0 ,lastLastModified: null,currentlyProcessed: [] };
        var self = this;
        
     /*   var actions ={
                        task: null,
                        removeFromProcessed:function(id){
                            var index = taskify.currentlyProcessed.indexOf(id);
                            if(index >= 0)
                            {
                                 taskify.currentlyProcessed.splice(index, 1);
                            }                            
                        },
                        active:function(cb){
                            if(!actions.task) return cb({error:"Empty task object"}, null );
                            actions.task.status = 'active';
                            actions.task.save(function(err,tsk){
                                actions.removeFromProcessed(tsk._id);
                                cb(null, actions );
                            });        
                        },                
                        failed: function(cb){
                            if(!actions.task) return cb({error:"Empty task object"}, null );
                            if(actions.task.get(options.retryField))
                            {
                                actions.task.set(options.failedTimesField, actions.task.get(options.retryTimesField) + 1 );
                                if( actions.task.get(options.retryTimesField) < actions.task.get(options.totalRetryTimesField) ){                                    
                                    actions.task.set(options.retryTimesField, actions.task.get(options.retryTimesField) + 1 )
                                    actions.task.status = 'active';       
                                }
                                else{
                                    actions.task.status = 'failed';       
                                }
                            }
                            else{
                                actions.task.status = 'failed';       
                            }
                            this.task.save(function(err,tsk){
                                actions.removeFromProcessed(tsk._id);
                                cb(null, actions );
                            }); 
                        },
                        completed: function(cb){
                            if(!actions.task) return cb({error:"Empty task object"}, null );
                            actions.task.status = 'completed';
                            actions.task.save(function(err,tsk){
                                actions.removeFromProcessed(tsk._id);
                                cb(null, actions );
                            }); 
                        }
                     };
        */
        function findTheTask(){
            var query = self.findOne({status: 'created'});
          /*  if(taskify['lastLastModified'])
            {
                //query.where(options.lastModifiedField).gte(taskify['lastLastModified']);
            }*/
            if( taskify['currentlyProcessed'].length > 0 )
            {
                query.where('_id').notin(self.watchingTasksIds);
            }                        
            query.sort('-'+options.lastModifiedField)
            
            query.exec(function(err,task){  
                    //console.log("find-task ",task);
                   if(!task)                    
                        return  taskify['findTimeout'] = setTimeout(findTheTask,1000);                    
                    else
                    {
                        clearTimeout(taskify['findTimeout']);
                        taskify['findTimeout'] = 0;
                        //taskify['lastLastModified'] = task.get(options.lastModifiedField);
                        //taskify['currentlyProcessed'].push(task._id);                                            
                        task.setStatusActive(function(err,ac){
                            cb(null,task);
                        });                        
                        return setTimeout(findTheTask,1000);                        
                    }
                });            
        }
        findTheTask();
        
    }
    schema.methods.addlog = function (codeID,message,relatedIDs,cb) {
        var self = this;
        var entry = {
                        code: codeID,
                        message: message,
                        relatedids: relatedIDs
                    };
        self.findByIdAndUpdate(
                        self._id,
                        {$push: {log: entry},lastModified:new Date() },
                        {safe: true, upsert: true},
                        function(err, task) {
                            if(err) return cb(err,null);
                            else return cb(null,task);
                        }
                    );
   
     }
    
   /*** schema.statics. = function(, cb) {
        
        
    }***/
     
     /****
     esto es lo que necesito
     
     guardar el objeto
     ====>OK ya se crea, y se guarda con los valores por defecto
     luego de guardarlo quedarme escuchando al objeto para ver si fue modificado, y que llame un callback cada vez que se modifique.
     =====>OK pero lo hago con una funcion diferente estatica, pero a la larga tiene el mismo efecto     
     suscribirme a todas los nuevos documentos, y llamar a una funcion que procese ese documento, si esta fucnion no responde en x segundos           
     
     agregar elementos al log
     actualizar el estado del progreso
     marcar como completado
     **/
     
}