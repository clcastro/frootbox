var mongoose = require('mongoose'),  
    BluePrint = require('./blueprint'),
    Minion = require('./minion'),
    Schema = mongoose.Schema;

var UserBluePrintSchema = new Schema({            
    blueprintbaseid: { type: Schema.ObjectId, ref: 'BluePrint' },
    created:Date,
    modified: Date,
    name: {type: String,default:""},
    slug: {type: String,default:""},
    minions:[ { type: Schema.ObjectId, ref: 'Minion' } ],
    minionsConfig:Object,
    maxPages: {type:Number,default:10},
    timeoutSeconds: {type:Number,default:300}    
});

module.exports = mongoose.model('UserBluePrint', UserBluePrintSchema);