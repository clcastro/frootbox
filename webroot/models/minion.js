var mongoose = require('mongoose'),    
    Schema = mongoose.Schema;

var MinionSchema = new Schema({                
    name: String,
    slug: String,
    url:String,
    isPageMinion:{type:Boolean,default:true},
    isSiteMinion:{type:Boolean,default:false},        
    defaultConfig:Array,
    resultRequiredScripts:Array,
    resultHtml:String,
    resultJS: String,
    resultCSS: String,
    scripts: String,
    css: String,
    defaultConfig: String,  
    examplePageResponse: String,
    exampleSiteResponse: String,    
    resultDefaultSize:{type:String,default:"2x2"},
    availableSizes:Array,
    created: Date,    
    lastModified: Date
});

module.exports = mongoose.model('Minion', MinionSchema);