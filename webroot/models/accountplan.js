var mongoose = require('mongoose'),
    Account = require('./account'),
    Plan = require('./plan'),
    Schema = mongoose.Schema;

var AccountPlanSchema = new Schema({    
    accountid : { type: Schema.ObjectId, ref: 'Account' },
    planid : { type: Schema.ObjectId, ref: 'Plan' },
    hooks: Array,
    programmedTasks: Array,    
});

module.exports = mongoose.model('AccountPlan', AccountPlanSchema);