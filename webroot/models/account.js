var mongoose = require('mongoose'),
    UserAccount = require('./useraccount'),
    AccountPlan = require('./accountplan'),
    Schema = mongoose.Schema;

var AccountSchema = new Schema({    
    created:Date,
    users:  [{ type: Schema.ObjectId, ref: 'UserAccount' }],
    plans:  [{ type: Schema.ObjectId, ref: 'AccountPlan' }]
});

module.exports = mongoose.model('Account', AccountSchema);