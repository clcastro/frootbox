var mongoose = require('mongoose'),
    /*BluePrint = require('./blueprint'),*/
    Schema = mongoose.Schema;

var PlanSchema = new Schema({        
    blueprintid:  { type: Schema.ObjectId, ref: 'BluePrint' },
    created:Date,
    lastModified:Date,
    type: {type:String,default:"free"}, /** It can be "free" or "premium" **/
    monthlyPrice: {type:Number,default:0},  /** **/
    yearlyPrice: {type:Number,default:0},  /** **/
    name: String,
    slug: String, /** For the url **/
    maxPagesPerTest:{type:Number,default:10},
    maxTestsPerDay:{type:Number,default:6},
    maxTestsPerDayPerDomain:{type:Number,default:3},
    minutesBetweenTests:{type:Number,default:30},
    canProgrammedTest: { type:Boolean, default:false },
    canHookTest: { type:Boolean, default:false },
    maxProgrammedTests: {type:Number,default:0},
    maxHookTests: {type:Number,default:0}
});

module.exports = mongoose.model('Plan', PlanSchema);