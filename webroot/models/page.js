var mongoose = require('mongoose'),
    Test = require('./test'),
    User = require('./user'),
    Taskify = require('./taskify'),
    Schema = mongoose.Schema;

var PageSchema = new Schema({    
    testid : { type: Schema.ObjectId, ref: 'Test' },
    userid : { type: Schema.ObjectId, ref: 'User' },    
    created: Date,
    completedOn: Date,
    status: String,
    url: String,     
    contentType: String,
    headers: Array,
    html: String,
    minions:Array
});

PageSchema.plugin(Taskify);

module.exports = mongoose.model('Page', PageSchema);