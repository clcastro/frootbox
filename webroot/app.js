var path = require('path'),
    express = require('express.io'),
    http = require('http'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = require('./models/user'),
    LocalStrategy = require('passport-local').Strategy,
    GoogleStrategy = require('passport-google').Strategy,
    FacebookStrategy = require('passport-facebook').Strategy;
    GitHubStrategy = require('passport-github').Strategy;

var FROOTCONFIG = require('config');
var logentries = require('node-logentries');

var log = logentries.logger({ token:FROOTCONFIG.log.token });

var app = express();

app.http().io()




// Configuration
app.set('views', __dirname + '/views');
app.engine('html', require('hogan-express'));
app.set('view engine', 'html');
app.set('layout', 'layouts/default');

//app.use(express.logger());
app.use(express.bodyParser());
app.use(express.methodOverride());

app.use(express.cookieParser());
app.use(express.session({secret: 'loremipsumpassworddolorsitamet' }));

app.use(passport.initialize());
app.use(passport.session());

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

app.configure('development', function(){
    app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
    app.locals.pretty = true;
});

app.configure('production', function(){
    app.use(express.errorHandler());
    app.locals.pretty = true;
});

// Configure passport
var User = require('./models/user');


//passport.use(new LocalStrategy(User.authenticate()));
passport.use(new LocalStrategy({  usernameField: 'email' },
  function(username, password, done) {
      console.log(arguments);
    User.findOne({ email: username }, function(err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect email.' });
      }
        user.authenticate(password,function(err,user,msg){
            if(err){
              return done(null, false, msg);
            }            
            else{
                return done(null, user);      
            }
        });
        /*if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }*/
      
    });
  }
));


passport.use(new GoogleStrategy({
    returnURL: FROOTCONFIG.general.baseurl + FROOTCONFIG.google.returnUrl,
    realm: FROOTCONFIG.general.baseurl
  },
  function(identifier, profile, done) {    
      
    User.findOne({email: profile.emails[0].value }, function(err,user){    
        if(err){
            done(err, user); 
        }
        else if(!user)
        {
            var user = User({ email:profile.emails[0].value,  name: {familyName: profile.name.familyName,givenName: profile.name.givenName } });            
            user.save(function(err,theuser){                                
                user.asignBasicPlanWithNewUser(function(err,acc){
                    done(null, theuser);
                })                
            });
        }else{         
           done(err, user); 
        }  
    });
      
  }
));


passport.use(new FacebookStrategy({
    clientID: FROOTCONFIG.facebook.clientID,
    clientSecret: FROOTCONFIG.facebook.clientSecret,
    callbackURL: FROOTCONFIG.general.baseurl + FROOTCONFIG.facebook.callbackUrl
  },
  function(accessToken, refreshToken, profile, done) {
      
    User.findOne({email: profile.emails[0].value }, function(err,user){
        if(err){
      
            done(err, user); 
        }
        else if(!user)
        {
            var user = User({ fbid : profile.id , name: {familyName: profile.last_name,givenName: profile.first_name}, email: profile.emails[0].value,fbMeta:profile });            
            
            user.asignBasicPlanWithNewUser(function(err,acc){
                done(null, theuser);
            });
        }else{
            if(user.fbid != profile.id){
                user.fbid = profile.id;
                user.fbMeta = profile;
                user.save(function(err,theuser){                                                  
                        done(null, theuser);
                  
                });
            }
            else{
                done(err, user);                 
            }
        }  
    });

  }

));

passport.use(new GitHubStrategy({
    clientID: "dc272777037f0d2e2a71",
    clientSecret: "df1f80cb268aaec178a93fb10e077353d20382cf",
    callbackURL: "http://www.frootbox.com/auth/github/callback"
  },
  function(accessToken, refreshToken, profile, done) {
      
      if(profile.emails[0].value){
            User.findOne({email: profile.emails[0].value }, function(err,user){
            if(err){      
                done(err, user); 
            }
            else if(!user)
            {
                var user = User({ email: profile.emails[0].value,githubUsername: profile.username,githubMeta:profile });            
                user.asignBasicPlanWithNewUser(function(err,acc){
                    done(null, theuser);
                });
            }else{
                if(user.githubId != profile.id){
                    user.githubId = profile.id;
                    user.githubUsername = profile.login;
                    user.githubMeta = profile;
                    user.save(function(err,theuser){
                        done(err, theuser);                
                    });
                }
                else{
                    done(err, user);                 
                }
            }  
        });                              
      }
      else{
          
            User.findOne({githubId: profile.id }, function(err,user){
            if(err){      
                done(err, user); 
            }
            else if(!user)
            {
                var user = User({ githubId: profile.id,githubUsername:profile.username, githubMeta:profile });            
                user.asignBasicPlanWithNewUser(function(err,acc){
                    done(null, theuser);
                });
            }else{
                if(user.githubId != profile.id){
                    user.githubId = profile.id;
                    user.githubUsername = profile.username;
                    user.githubMeta = profile;
                    user.save(function(err,theuser){
                        done(err, theuser);                
                    });
                }
                else{
                    done(err, user);                 
                }
            }  
        });                              
      
      }  
  }
));


passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());



// Connect mongoose
mongoose.connect(FROOTCONFIG.db.mongo);

// Setup routes
require('./routes')(app);
app.listen(FROOTCONFIG.general.port);
/*
http.createServer(app).listen(FROOTCONFIG.general.port, '0.0.0.0', function() {
    console.log("Express server listening on %s:%d in %s mode", '0.0.0.0', FROOTCONFIG.general.port, app.settings.env);
    //log.alert("[app-restart] 0.0.0.0:",FROOTCONFIG.general.port," in ",app.settings.env," mode");
});*/
